﻿var ComputerCompanyOverview = (function () {
    function configureAddComputerClick() {
        $(document).on("click", "#btnAddComputer", function () {
            location.href = companyDetailUrl;
        });
    }

    function configureDeleteComputerAction(computerId) {
        NmbrsComputerService.deleteComputerById(computerId)
            .then(() => {
                $('#tblComputers').empty();
                loadTableComputers();
            })
            .catch((error) => {
                alert(error.responseText);
            })
    }

    function configureDeleteComputerButtons() {
        $(".delete").each(function () {
            $(this).click(function () {
                if (confirm("Are you sure you want to delete this computer?")) {
                    var rowId = $(this).attr("rowID");
                    configureDeleteComputerAction(parseInt(rowId));
                }
            });
        });
    }

    function loadTableComputers() {
        NmbrsComputerService.getAllComputersByTenant()
            .then((data) => {
                let table = document.getElementById("tblComputers");
                let header = Object.keys(data[0]);

                generateTable(table, data, true);
                generateTableHead(table, header);
                configureDeleteComputerButtons();
            })
            .catch((error) => {
                console.log(error)
            });
    }

    function init() {
        loadTableComputers();
        configureAddComputerClick();
    }

    return {
        init: init
    };
}());

ComputerCompanyOverview.init();
