const baseUrl = 'https://localhost:44300/api/computer/';
const companyDetailUrl = 'https://localhost:44307/feature/company/detail/';
const companyOverviewUrl = 'https://localhost:44307/feature/company/overview';

const tenantId = '3fa85f64-5717-4562-b3fc-2c963f66afa6';
const companyId = 3;
const employeeId = 7;

class Mappable {
	constructor(keys, values) {
		this.keys = keys;
		this.values = values;
		this.map = new Map();
		for (var i = 0; i < keys.length; i++) {
			this.map.set(keys[i], values[i]);
		}
	}

	getValue(val) {
		return this.map.get(val);
	}
}

class Brand extends Mappable {
	constructor() {
		let brandKeys = [1, 2, 3];
		let brandValues = ["Dell", "Apple", "Lenovo"];

		super(brandKeys, brandValues);
	}
}

class Cpu extends Mappable {
	constructor() {
		let cpuKeys = [1, 2, 3];
		let cpuValues = ["I5", "I7", "I9"];

		super(cpuKeys, cpuValues);
	}
}

class Enabled extends Mappable {
	constructor() {
		let enabledKeys = [1, 0];
		let enabledValues = ["true", "false"];

		super(enabledKeys, enabledValues);
	}
}
function loadEnumDropdown(controlId, enumClass) {
    for (var key of enumClass.map.keys()) {
        $("#" + controlId).append("<option value=" + key + ">" + enumClass.map.get(key) + "</option>");
    }
}

function generateTableHead(table, data) {
    let thead = table.createTHead();
    let row = thead.insertRow();
    for (let key of data) {
        let th = document.createElement("th");
        let text = document.createTextNode(key);
        th.appendChild(text);
        row.appendChild(th);
    }
}
var editButton = "<td><a class='nav-link active'' href='https://localhost:44307/feature/company/detail?id={ID}'>Edit</a></td>";
var deleteButton = "<td><a class='nav-link active delete'' rowId ='{ID}' href='#'>Delete</a></td>";
var assignButton = "<td><a class='nav-link active assign' isEnabled='{ENABLED}' rowId ='{ID}' href='#'>Assign</a></td>";

function getButtonWithId(button, id) {
    return button.replace("{ID}", id);
}

function addEnabledAttributeValue(button, value) {
    return button.replace("{ENABLED}", value);
}

function generateTable(table, data, isCompany) {
    for (let element of data) {
        let row = table.insertRow();
        var id = 0;
        var isEnabled = false;
        for (let key in element) {
            let val = element[key];
            if (key == 'id') {
                id = element[key];
            } else if (key == 'brand') {
                val = (new Brand()).getValue(val);
            }
            else if (key == 'cpu') {
                val = (new Cpu()).getValue(val);
            }
            else if (key == 'isEnabled') {
                isEnabled = val;
            }
            let cell = row.insertCell();
            let text = document.createTextNode(val);
            cell.appendChild(text);
        }
        if (isCompany) {
            let editCell = row.insertCell();
            editCell.outerHTML = getButtonWithId(editButton, id);
            let deleteCell = row.insertCell();
            deleteCell.outerHTML = getButtonWithId(deleteButton, id);
        } else {
            let assignCell = row.insertCell();
            assignCell.outerHTML = getButtonWithId(addEnabledAttributeValue(assignButton, isEnabled), id);
        }
    }
}

function getParameterFromRequestUrl(name) {
    if (name = (new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]*)')).exec(location.search))
        return decodeURIComponent(name[1]);
}

var NmbrsComputerService = (function () {
    function ajaxRequest(url, type, data) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: url,
                type: type,
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify(data),
                success: function (result) {
                    resolve(result)
                },
                error: function (error) {
                    reject(error)
                },
            })
        });
    }

    function getAllComputersByTenant() {
        return ajaxRequest(baseUrl + tenantId, 'GET', null);
    }

    function deleteComputerById(id) {
        return ajaxRequest(baseUrl + tenantId + '/' + id, 'DELETE', null);
    }

    function updateComputer(url, actionType, data) {
        return ajaxRequest(url, actionType, data);
    }

    function getComputerById(id) {
        return ajaxRequest(baseUrl + tenantId + '/' + id, 'GET', null);
    }

    return {
        getAllComputersByTenant: getAllComputersByTenant,
        deleteComputerById: deleteComputerById,
        updateComputer: updateComputer,
        getComputerById: getComputerById
    };
}());
var ComputerCompanyOverview = (function () {
    function configureAddComputerClick() {
        $(document).on("click", "#btnAddComputer", function () {
            location.href = companyDetailUrl;
        });
    }

    function configureDeleteComputerAction(computerId) {
        NmbrsComputerService.deleteComputerById(computerId)
            .then(() => {
                $('#tblComputers').empty();
                loadTableComputers();
            })
            .catch((error) => {
                alert(error.responseText);
            })
    }

    function configureDeleteComputerButtons() {
        $(".delete").each(function () {
            $(this).click(function () {
                if (confirm("Are you sure you want to delete this computer?")) {
                    var rowId = $(this).attr("rowID");
                    configureDeleteComputerAction(parseInt(rowId));
                }
            });
        });
    }

    function loadTableComputers() {
        NmbrsComputerService.getAllComputersByTenant()
            .then((data) => {
                let table = document.getElementById("tblComputers");
                let header = Object.keys(data[0]);

                generateTable(table, data, true);
                generateTableHead(table, header);
                configureDeleteComputerButtons();
            })
            .catch((error) => {
                console.log(error)
            });
    }

    function init() {
        loadTableComputers();
        configureAddComputerClick();
    }

    return {
        init: init
    };
}());

ComputerCompanyOverview.init();