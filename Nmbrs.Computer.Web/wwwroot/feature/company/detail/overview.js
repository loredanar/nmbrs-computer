﻿var ComputerCompanyDetail = (function () {
    function loadBrandDropdown() {
        loadEnumDropdown("brand", new Brand())
    }

    function loadCpuDropdown() {
        loadEnumDropdown("cpu", new Cpu())
    }

    function loadEnabledDropdown() {
        loadEnumDropdown("isEnabled", new Enabled())
    }

    function loadDropdowns() {
        loadBrandDropdown();
        loadCpuDropdown();
        loadEnabledDropdown();
    }

    var isEdit;
    function setIsEdit() {
        if (/[?&]id=/.test(location.search)) {
            isEdit = true;
        }
    }

    function isEnabled() {
        if ($("#isEnabled").val() == "1")
            return true;
        return false;
    }

    function parseIsEnabled(val) {
        if (val)
            return 1;
        return 0;
    }

    function getFormData() {
        return {
            "uniqueId": $("#uniqueId").val(),
            "companyId": companyId,
            "brand": parseInt($("#brand").val()),
            "cpu": parseInt($("#cpu").val()),
            "employeeId": null,
            "isEnabled": isEnabled(),
            "startPeriod": parseInt($("#startPeriod").val()),
            "startYear": parseInt($("#startYear").val()),
            "endPeriod": parseInt($("#endPeriod").val()),
            "endYear": parseInt($("#endYear").val())
        };
    }

    function setBtnSubmitAction(actionType, url) {
        $("#btnSubmitComputer").click(function () {
            var data = getFormData();
            NmbrsComputerService.updateComputer(url, actionType, data)
                .then(() => {
                    window.location.href = companyOverviewUrl;
                })
                .catch((error) => {
                    alert(error.responseText);
                });
        });
    }

    function configureSubmitComputerData() {
        var actionType = "POST"
        var url = baseUrl + tenantId.toString();
        if (isEdit) {
            actionType = "PATCH";
            url += '/'+ getParameterFromRequestUrl('id');
        }
        setBtnSubmitAction(actionType, url);
    }

    function loadEditModel() {
        var id = getParameterFromRequestUrl('id');
        NmbrsComputerService.getComputerById(id)
            .then((data) => {
                $("#uniqueId").val(data.uniqueId);
                $("#brand").val(data.brand);
                $("#cpu").val(data.cpu);
                $("#isEnabled").val(parseIsEnabled(data.isEnabled));
                $("#startPeriod").val(data.startPeriod);
                $("#startYear").val(data.startYear);
                $("#endPeriod").val(data.endPeriod);
                $("#endYear").val(data.endYear);
            })
            .catch((error) => {
                console.log(error)
            })
    }

    function init() {
        setIsEdit();
        loadDropdowns();
        configureSubmitComputerData();
        if (isEdit) {
            loadEditModel();
        }
    }

    return {
        init: init
    };
}());

ComputerCompanyDetail.init();
