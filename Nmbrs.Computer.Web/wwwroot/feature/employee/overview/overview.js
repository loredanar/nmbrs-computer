var ComputerEmployeeOverview = (function () {
    function configureAssignComputerData(computerId) {
        var data = {
            "computerId": computerId,
            "employeeId": employeeId
        };
        
        NmbrsComputerService.updateComputer(baseUrl + tenantId, "PATCH", data)
            .then(() => {
                location.reload();
            })
            .catch((error) => {
                alert(error.responseText);
            });
    }

    function init() {
        NmbrsComputerService.getAllComputersByTenant()
            .then((data) => {
                let table = document.getElementById("tblComputers");
                let header = Object.keys(data[0]);

                generateTable(table, data, false);
                generateTableHead(table, header);

                $(".assign").each(function () {
                    var isEnabled = $(this).attr("isEnabled");
                    if (isEnabled == 'true') {
                        $(this).click(function () {
                            var rowId = $(this).attr("rowId");
                            configureAssignComputerData(parseInt(rowId));
                        });
                    }
                    else {
                        $(this).addClass('disabled');
                    }
                 
                });
            })
            .catch((error) => {
                console.log(error)
            });
    }

    return {
        init: init
    };
}());

ComputerEmployeeOverview.init();
