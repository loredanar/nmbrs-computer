﻿class Mappable {
	constructor(keys, values) {
		this.keys = keys;
		this.values = values;
		this.map = new Map();
		for (var i = 0; i < keys.length; i++) {
			this.map.set(keys[i], values[i]);
		}
	}

	getValue(val) {
		return this.map.get(val);
	}
}

class Brand extends Mappable {
	constructor() {
		let brandKeys = [1, 2, 3];
		let brandValues = ["Dell", "Apple", "Lenovo"];

		super(brandKeys, brandValues);
	}
}

class Cpu extends Mappable {
	constructor() {
		let cpuKeys = [1, 2, 3];
		let cpuValues = ["I5", "I7", "I9"];

		super(cpuKeys, cpuValues);
	}
}

class Enabled extends Mappable {
	constructor() {
		let enabledKeys = [1, 0];
		let enabledValues = ["true", "false"];

		super(enabledKeys, enabledValues);
	}
}