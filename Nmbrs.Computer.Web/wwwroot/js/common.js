﻿function loadEnumDropdown(controlId, enumClass) {
    for (var key of enumClass.map.keys()) {
        $("#" + controlId).append("<option value=" + key + ">" + enumClass.map.get(key) + "</option>");
    }
}

function generateTableHead(table, data) {
    let thead = table.createTHead();
    let row = thead.insertRow();
    for (let key of data) {
        let th = document.createElement("th");
        let text = document.createTextNode(key);
        th.appendChild(text);
        row.appendChild(th);
    }
}
var editButton = "<td><a class='nav-link active'' href='https://localhost:44307/feature/company/detail?id={ID}'>Edit</a></td>";
var deleteButton = "<td><a class='nav-link active delete'' rowId ='{ID}' href='#'>Delete</a></td>";
var assignButton = "<td><a class='nav-link active assign' isEnabled='{ENABLED}' rowId ='{ID}' href='#'>Assign</a></td>";

function getButtonWithId(button, id) {
    return button.replace("{ID}", id);
}

function addEnabledAttributeValue(button, value) {
    return button.replace("{ENABLED}", value);
}

function generateTable(table, data, isCompany) {
    for (let element of data) {
        let row = table.insertRow();
        var id = 0;
        var isEnabled = false;
        for (let key in element) {
            let val = element[key];
            if (key == 'id') {
                id = element[key];
            } else if (key == 'brand') {
                val = (new Brand()).getValue(val);
            }
            else if (key == 'cpu') {
                val = (new Cpu()).getValue(val);
            }
            else if (key == 'isEnabled') {
                isEnabled = val;
            }
            let cell = row.insertCell();
            let text = document.createTextNode(val);
            cell.appendChild(text);
        }
        if (isCompany) {
            let editCell = row.insertCell();
            editCell.outerHTML = getButtonWithId(editButton, id);
            let deleteCell = row.insertCell();
            deleteCell.outerHTML = getButtonWithId(deleteButton, id);
        } else {
            let assignCell = row.insertCell();
            assignCell.outerHTML = getButtonWithId(addEnabledAttributeValue(assignButton, isEnabled), id);
        }
    }
}

function getParameterFromRequestUrl(name) {
    if (name = (new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]*)')).exec(location.search))
        return decodeURIComponent(name[1]);
}