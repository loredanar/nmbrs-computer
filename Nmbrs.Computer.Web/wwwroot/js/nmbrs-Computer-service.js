﻿
var NmbrsComputerService = (function () {
    function ajaxRequest(url, type, data) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: url,
                type: type,
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify(data),
                success: function (result) {
                    resolve(result)
                },
                error: function (error) {
                    reject(error)
                },
            })
        });
    }

    function getAllComputersByTenant() {
        return ajaxRequest(baseUrl + tenantId, 'GET', null);
    }

    function deleteComputerById(id) {
        return ajaxRequest(baseUrl + tenantId + '/' + id, 'DELETE', null);
    }

    function updateComputer(url, actionType, data) {
        return ajaxRequest(url, actionType, data);
    }

    function getComputerById(id) {
        return ajaxRequest(baseUrl + tenantId + '/' + id, 'GET', null);
    }

    return {
        getAllComputersByTenant: getAllComputersByTenant,
        deleteComputerById: deleteComputerById,
        updateComputer: updateComputer,
        getComputerById: getComputerById
    };
}());