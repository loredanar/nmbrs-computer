using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Nmbrs.Computer.Service.Core.Services.LanguageText;
using Nmbrs.Infrastructure.EnumsAndConstants.Enums;
using Nmbrs.Infrastructure.Log.Interfaces;
using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Hosting;

namespace Nmbrs.Computer.Service.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/LanguageText")]
    //[Authorize]
    public class LanguageTextController : BaseController
    {
        private readonly ILanguageTextService _languageTextService;
        public LanguageTextController(ILanguageTextService languageTextService, IWebHostEnvironment env, IMediator mediator, ILogger logger, IMapper mapper) : base(env, mediator, logger, mapper)
        {
            _languageTextService = languageTextService;
        }

        [HttpGet("{language}")]
        public async Task<IActionResult> Get(string language)
        {
            _logger.Information(Internal.Constants.APPLICATION_NAME, "Get LanguageText keys by language");

            Language lang;
            Enum.TryParse(language.ToUpper(), out lang);

            var texts = await _languageTextService.Get(lang);

            var result = new JObject();
            foreach (var languageText in texts)
            {
                if (!result.ContainsKey(languageText.key))
                {
                    result.Add(languageText.key, languageText.text);
                }
            }

            return Ok(result);
        }
    }
}
