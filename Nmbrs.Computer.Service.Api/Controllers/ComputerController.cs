using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Nmbrs.Computer.Service.Api.Models.DTO;
using Nmbrs.Computer.Service.Core.Commands;
using Nmbrs.Computer.Service.Core.Queries;
using Nmbrs.Infrastructure.Log.Interfaces;
using Nmbrs.Security.Policies;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nmbrs.Computer.Service.Api.Controllers
{
    /// <summary>
    /// Example of a controller with CQRS
    /// </summary>
    [Produces("application/json")]
    //[Route("api/{tenantId}/{userAccountId}/computer"), Authorize]
    [AllowAnonymous]
    [Route("api/[controller]")]
    public class ComputerController : BaseController
    {
        private readonly IComputerQueries _COMPUTERQueries;
        public ComputerController(IComputerQueries COMPUTERQueries, IWebHostEnvironment env, IMediator mediator, ILogger logger, IMapper mapper) : base(env, mediator, logger, mapper)
        {
            _COMPUTERQueries = COMPUTERQueries;
        }
        [HttpGet("{tenantId}")]
        [Authorize(Policy = PolicyName.TenantRequired)]
        public Task<IActionResult> GetAll(Guid tenantId)
        {
            return Retrieve<IReadOnlyCollection<Service.Core.Queries.Models.Computer>, IReadOnlyCollection<Service.Core.Queries.Models.Computer>>(() => _COMPUTERQueries.GetAll(tenantId, Instance));
        }
        [HttpGet("{tenantId}/{id}")]
        [Authorize(Policy = PolicyName.TenantRequired)]
        public Task<IActionResult> Get(Guid tenantId, int id)
        {
            return Retrieve<Service.Core.Queries.Models.Computer, Service.Core.Queries.Models.Computer>(() => _COMPUTERQueries.GetByTenantIdAndComputerId(tenantId, id));
        }
        [HttpPost("{tenantId}")]
        //[Authorize(Policy = PolicyName.TenantRequired)]
        public async Task<IActionResult> Create(Guid tenantId, [FromBody] CreateComputerDTO model)
        {
            if (model == null)
                return BadRequest($"Please provide a {nameof(CreateComputerDTO)}");

            var command = new CreateComputerCommand(
                    model.UniqueId,
                    model.CompanyId,
                    tenantId,
                    model.Brand,
                    model.Cpu,
                    model.EmployeeId,
                    model.IsEnabled,
                    model.StartPeriod,
                    model.StartYear,
                    model.EndPeriod,
                    model.EndYear
                );
            command.SetInstance(Instance);

            return RetrieveCommandResult(await _mediator.Send(command));
        }

        [HttpPatch("{tenantId}/{id}")]
        [Authorize(Policy = PolicyName.TenantRequired)]
        public async Task<IActionResult> Update(Guid tenantId, int id, [FromBody] UpdateComputerDTO model)
        {
            if (model == null)
                return BadRequest($"Please provide an {nameof(UpdateComputerDTO)}");

            var command = new UpdateComputerCommand(
                    id,
                    model.UniqueId,
                    model.CompanyId,
                    tenantId,
                    model.Brand,
                    model.Cpu,
                    model.EmployeeId,
                    model.IsEnabled,
                    model.StartPeriod,
                    model.StartYear,
                    model.EndPeriod,
                    model.EndYear
                );
            
            return RetrieveCommandResult(await _mediator.Send(command));
        }

        [HttpPatch("{tenantId}")]
        [Authorize(Policy = PolicyName.TenantRequired)]
        public async Task<IActionResult> AssignComputer(Guid tenantId, [FromBody] AssignComputerDTO model)
        {
            if (model == null)
                return BadRequest($"Please provide an {nameof(AssignComputerDTO)}");

            var computer = await _COMPUTERQueries.GetByTenantIdAndComputerId(tenantId, model.ComputerId);

            var command = new UpdateComputerCommand(
                    computer.Id,
                    computer.UniqueId,
                    computer.CompanyId,
                    tenantId,
                    computer.Brand,
                    computer.Cpu,
                    model.EmployeeId,
                    computer.IsEnabled,
                    computer.StartPeriod,
                    computer.StartYear,
                    computer.EndPeriod,
                    computer.EndYear
                );

            return RetrieveCommandResult(await _mediator.Send(command));
        }

        [HttpDelete("{tenantId}/{id}")]
        [Authorize(Policy = PolicyName.TenantRequired)]
        public async Task<IActionResult> Delete(Guid tenantId, int id)
        {
            var computer = await _COMPUTERQueries.GetByTenantIdAndComputerId(tenantId, id);

            if (computer.EmployeeId.HasValue)
                return BadRequest($"Computer is assigned to an employee and can't be deleted!");

            var command = new DeleteComputerCommand(id);

            return RetrieveCommandResult(await _mediator.Send(command));
        }
    }
}
