using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Nmbrs.Infrastructure.Log.Interfaces;
using Nmbrs.Computer.Service.Api.Models;
using System;
using System.Threading.Tasks;
using Nmbrs.Computer.Service.Core;
using Nmbrs.Commands.Core.Result;

namespace Nmbrs.Computer.Service.Api.Controllers
{
    public abstract class BaseController : Controller
    {
        private const string INSTANCE_HEADER = "x-nmbrs-instance";
        protected readonly IMapper _mapper;
        protected readonly IWebHostEnvironment _env;
        protected readonly IMediator _mediator;
        protected readonly ILogger _logger;
        protected string Instance
        {
            get
            {
                if (_env.IsEnvironment("kitchen"))
                {
                    string header = HttpContext.Request.Headers[INSTANCE_HEADER];
                    if (string.IsNullOrWhiteSpace(header))
                        throw new InstanceException($"{INSTANCE_HEADER} header was not provided in the Request towards a kitchen instance of Computer Service!");
                    return header;
                }
                return null;
            }
        }
        protected BaseController(IWebHostEnvironment env, IMediator mediator, ILogger logger, IMapper mapper)
        {
            _env = env;
            _mediator = mediator;
            _logger = logger;
            _mapper = mapper;
        }
        protected virtual Task<IActionResult> Retrieve<T, TDestination>(Func<Task<T>> func) where T : class
            => Retrieve(func, (result) => _mapper.Map<TDestination>(result));
        protected virtual async Task<IActionResult> Retrieve<T, TDestination>(Func<Task<T>> func, Func<T, TDestination> mapFunc) where T : class
        {
            try
            {
                var result = await func();
                if (result == null)
                    return NotFound();
                return Ok(mapFunc(result));
            }
            catch (Exception ex)
            {
                _logger.Error(nameof(Retrieve), "Exception occured while retrieving data from Computer Service", exception: ex);
                throw;
            }
        }

        protected IActionResult RetrieveCommandResult(CommandResult commandResult)
        {
            Guard.NotNull(commandResult, nameof(commandResult));

            if (!commandResult.IsSuccessful)
            {
                return StatusCode(commandResult.StatusCode, commandResult.Value);
            }

            return Ok(commandResult.Value);
        }
    }
}

