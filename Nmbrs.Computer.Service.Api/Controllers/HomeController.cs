using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Nmbrs.TEMPLATE.Service.Api.Controllers
{
    [Produces("application/json")]
    [Route("/")]
    public class HomeController : Controller
    {
        [HttpGet]
        [AllowAnonymous]
        public string Home()
        {
            return "Hello World"; //Just to show everything is running
        }
    }
}
