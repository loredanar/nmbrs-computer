using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nmbrs.Computer.Service.Api.Models;
using Nmbrs.Vault.Extensions;
using Nmbrs.Vault.Extensions.Models;
using Nmbrs.HealthChecks;
using Nmbrs.HealthChecks.ApplicationInsights;

namespace Nmbrs.Computer.Service.Api.HealthChecks
{
    public static class HealthChecksExtensions
    {
        public static IServiceCollection AddHealthChecks(this IServiceCollection services, IConfiguration configuration, IWebHostEnvironment env, ServiceConfig vaultSecrets)
        {
            //var generalVault = new VaultHelper(configuration, env.EnvironmentName, new VaultHelperOptions { VaultNameKey = "GeneralVaultName" }).GetVault();
            services.AddHealthChecks()
                    //.AddSqlServer(vaultSecrets.DbConnectionString, name: HealthCheckName.SqlServer.WithDetails(vaultSecrets.DbConnectionString.Split(';')[0].Split('=')[1]))
                    //.AddAzureKeyVault(v => v.SetComputerVault(configuration, env, vaultSecrets), HealthCheckName.AzureKeyVault.WithDetails(configuration["VaultName"]), HealthStatus.Degraded)
                    //.AddAzureKeyVault(v => v.SetGeneralVault(vaultSecrets, env), HealthCheckName.AzureKeyVault.WithDetails(configuration["GeneralVaultName"]), HealthStatus.Degraded)
                    //.AddIdentityServer(new Uri(vaultSecrets.IdentityServiceUrl), HealthCheckName.IdentityServer.WithDetails(vaultSecrets.IdentityServiceUrl))
                    //.AddAzureServiceBusQueue(vaultSecrets.MessagingServiceBusConnectionString, vaultSecrets.QueueSyncDataName, HealthCheckName.AzureServiceBusQueue.WithDetails(vaultSecrets.QueueSyncDataName))
                    //.AddAzureServiceBusQueue(vaultSecrets.MessagingServiceBusConnectionString, vaultSecrets.MessagingQueueDataRequest, HealthCheckName.AzureServiceBusQueue.WithDetails(vaultSecrets.MessagingQueueDataRequest))
                    //.AddAzureServiceBusTopic(vaultSecrets.MessagingServiceBusConnectionString, vaultSecrets.MessagingTopicDataResponse, HealthCheckName.AzureServiceBusTopic.WithDetails(vaultSecrets.MessagingTopicDataResponse))
                    //.AddTableStorageChecks(vaultSecrets.StorageAccountName, vaultSecrets.StorageAccountKey)
                    .AddApplicationInsightsPublisher();
            return services;
        }
    }
}
