using AutoMapper;
using Nmbrs.Computer.Service.Api.Models.DTO;
using Service = Nmbrs.Computer.Service;


namespace Nmbrs.Computer.Service.Api.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Service.Core.Queries.Models.Computer, ComputerDTO>().ReverseMap().ReverseMap();
            CreateMap<ComputerDTO, Service.Core.Queries.Models.Computer>().ReverseMap().ReverseMap();
        }
    }
}
