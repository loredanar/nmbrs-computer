using GitHub;
using Nmbrs.Infrastructure.Log.Domain;
using Nmbrs.Infrastructure.Log.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nmbrs.Computer.Service.Api.Internal
{
    public class RefactoringLogger : IResultPublisher
    {
        public const string EMBEDDED_OBJECTS = "embeddedObjects";
        public const string DURATION_DIF_MULTIPLIER = "DurantionDiferenceMultiplier";
        private const string LOG_CATEGORY = "Scientist";
        public const string LOG_FIELD__TENANT = "tenant";
        public const string LOG_FIELD_USER = "user";
        private const string LOG_FIELD_EXPERIENCE_NAME = "ExperimentName";
        protected readonly ILogger _logger;
        public RefactoringLogger(ILogger logger)
        {
            _logger = logger;
        }
        public Task Publish<T, TClean>(Result<T, TClean> result)
        {
            if (result.Mismatched)
            {
                var embeddedObjects = result.Contexts.ContainsKey(EMBEDDED_OBJECTS) ? result.Contexts[EMBEDDED_OBJECTS] : new Dictionary<string, object>();
                var tenant = result.Contexts.ContainsKey(LOG_FIELD__TENANT) ? result.Contexts[LOG_FIELD__TENANT] : new Infrastructure.Log.Domain.Tenant { ID = "0" };
                embeddedObjects.Add("Candidates", result.Candidates);
                embeddedObjects.Add("Control", result.Control);
                embeddedObjects.Add(LOG_FIELD_EXPERIENCE_NAME, result.ExperimentName);
                embeddedObjects.Add("Context", result.Contexts);
                _logger.Debug(LOG_CATEGORY, "Result Mismatched ({@ExperimentName})", embeddedObjects: embeddedObjects, tenant: tenant);
            }
            //check for long durations
            if (result.Contexts.ContainsKey(DURATION_DIF_MULTIPLIER) && result.Contexts[DURATION_DIF_MULTIPLIER] is int)
            {
                var baseDuration = result.Control.Duration.Ticks;
                foreach (var candidate in result.Candidates)
                {
                    if (candidate.Duration.Ticks > baseDuration * result.Contexts[DURATION_DIF_MULTIPLIER])
                    {
                        var embeddedObjects = result.Contexts.ContainsKey(EMBEDDED_OBJECTS) ? result.Contexts[EMBEDDED_OBJECTS] : new Dictionary<string, object>();
                        var user = result.Contexts.ContainsKey(LOG_FIELD_USER) ? result.Contexts[LOG_FIELD_USER] : new User { ID = "0" };
                        var tenant = result.Contexts.ContainsKey(LOG_FIELD__TENANT) ? result.Contexts[LOG_FIELD__TENANT] : new Infrastructure.Log.Domain.Tenant { ID = "0" };
                        embeddedObjects.Add("CandidateName", candidate.Name);
                        embeddedObjects.Add("ControlDuration", result.Control.Duration);
                        embeddedObjects.Add("CandidateDuration", candidate.Duration);
                        embeddedObjects.Add(LOG_FIELD_EXPERIENCE_NAME, result.ExperimentName);
                        _logger.Debug(LOG_CATEGORY, "Candidate too slow ({@ExperimentName})", embeddedObjects: embeddedObjects, tenant: tenant, user: user);
                    }
                }
            }
            return Task.FromResult(0);
        }
    }
}
