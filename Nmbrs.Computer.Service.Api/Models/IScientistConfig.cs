namespace Nmbrs.Computer.Service.Api.Models
{
    interface IScientistConfig
    {
        bool RunScientist { get; set; }
    }
}
