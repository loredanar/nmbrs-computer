using Nmbrs.Infrastructure.Log.Enums;

namespace Nmbrs.Computer.Service.Api.Models
{
    public class NmbrsLogConfig
    {
        public bool IsEnabled { get; set; }
        public LogEventLevel MinimumLevel { get; set; }
    }
}
