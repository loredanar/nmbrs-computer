using MassTransit.AzureServiceBusTransport;
using Nmbrs.Security.APIKey.Interfaces;
using Nmbrs.Computer.Service.Core.Services.Interfaces;

namespace Nmbrs.Computer.Service.Api.Models
{
    public class ServiceConfig : IAPIKeysOptions, IScientistConfig, IComputerStorage
    {
        public string IdentityServiceUrl { get; set; }
        public string APIKeysWhitelist { get; set; }
        public bool RunScientist { get; set; }
        public string SbConnectionString { get; set; }  //remove if it's NO Messaging
        public string AzureProviderConfigUserName { get; set; }  //remove if it's NO Azure Tables
        public string AzureProviderConfigPassword { get; set; }  //remove if it's NO Azure Tables
        public string DbConnectionString { get; set; }
    }
}
