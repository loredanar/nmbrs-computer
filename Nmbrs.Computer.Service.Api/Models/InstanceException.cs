using System;
using System.Runtime.Serialization;

namespace Nmbrs.Computer.Service.Api.Models
{
    [Serializable]
    public class InstanceException : Exception
    {
        public InstanceException() { }
        public InstanceException(string message) : base(message) { }
        public InstanceException(string message, Exception innerException) : base(message, innerException) { }
        protected InstanceException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
