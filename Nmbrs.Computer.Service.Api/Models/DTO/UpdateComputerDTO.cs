using System;

namespace Nmbrs.Computer.Service.Api.Models.DTO
{
    public class UpdateComputerDTO
    {
        public string UniqueId { get; set; }
        public int CompanyId { get; set; }
        public int Brand { get; set; }
        public int Cpu { get; set; }
        public int? EmployeeId { get; set; }
        public bool IsEnabled { get; set; }
        public int StartPeriod { get; set; }
        public int StartYear { get; set; }
        public int EndPeriod { get; set; }
        public int EndYear { get; set; }
    }
}
