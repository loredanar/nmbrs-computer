﻿
namespace Nmbrs.Computer.Service.Api.Models.DTO
{
    public class AssignComputerDTO
    {
        public int ComputerId { get; set; }
        public int? EmployeeId { get; set; }
    }
}
