using Microsoft.Extensions.Configuration;

namespace Nmbrs.Computer.Service.Api.Config
{
    public static class Extensions
    {
        public static bool UseAPIKey(this IConfiguration configuration)
        {
            return configuration.GetValue<bool>("UseAPIKey");
        }
    }
}
