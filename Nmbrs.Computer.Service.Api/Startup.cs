using AutoMapper;
using CorrelationId.Abstractions;
using CorrelationId.DependencyInjection;
using GlobalExceptionHandler.WebApi;
using HealthChecks.UI.Client;
using MediatR;
using MediatR.Extensions.FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Nmbrs.Communication.Extensions.HeaderPropagation;
using Nmbrs.Computer.Service.Api.Config;
using Nmbrs.Computer.Service.Api.HealthChecks;
using Nmbrs.Computer.Service.Api.Internal;
using Nmbrs.Computer.Service.Api.Mappings;
using Nmbrs.Computer.Service.Api.Models;
using Nmbrs.Computer.Service.Core.Commands;
using Nmbrs.Computer.Service.Core.Commands.Behaviors;
using Nmbrs.Computer.Service.Core.Commands.Validators;
using Nmbrs.Computer.Service.Core.Mappings;
using Nmbrs.Computer.Service.Core.Queries;
using Nmbrs.Computer.Service.Core.Services;
using Nmbrs.Computer.Service.Core.Services.Interfaces;
using Nmbrs.Computer.Service.Core.Services.LanguageText;
using Nmbrs.Computer.Service.Data;
using Nmbrs.HealthChecks;
using Nmbrs.HealthChecks.HealthPing.Extensions;
using Nmbrs.Infrastructure.Log.Enums;
using Nmbrs.Infrastructure.Log.Interfaces;
using Nmbrs.Infrastructure.Log.Loggly;
using Nmbrs.Infrastructure.Log.Loggly.Configuration.Default;
using Nmbrs.Security.APIKey;
using Nmbrs.Security.Policies;
using Nmbrs.Vault.Extensions;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;

namespace Nmbrs.Computer.Service.Api
{
    public class Startup
    {
        private readonly IWebHostEnvironment _env;
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            _env = env;
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            ServiceConfig config = Task.Run(() => new VaultHelper(Configuration, _env.EnvironmentName).GetConfiguration<ServiceConfig>()).Result;

            var instanceId = Environment.GetEnvironmentVariable("WEBSITE_INSTANCE_ID");

            NmbrsLogConfig nmbrsLogConfig = Configuration.GetSection("NmbrsLogConfig").Get<NmbrsLogConfig>();
            var defaultLoggerConfig = new DefaultLogglyLoggerConfiguration()
            {
                ApplicationName = _env.ApplicationName,
                Environment = _env.EnvironmentName,
                MinimumLevel = nmbrsLogConfig.MinimumLevel,
                IsEnabled = nmbrsLogConfig.IsEnabled,
                AppType = AppType.Services,
                InstanceId = string.IsNullOrEmpty(instanceId) ? "localInstance" : instanceId
            };

            services.AddMemoryCache();

            if (Configuration.UseAPIKey())
            {
                services.AddInMemoryAPIKeyServices(config);
            }

            if (!Debugger.IsAttached)
            {
                services.AddApplicationInsightsTelemetry();
                services.AddHealthChecks(Configuration, _env, config);
                services.AddHealthTracking();
            }

            services.AddHttpContextAccessor();
            services.AddCorrelationId();

            services.AddSingleton<ILogger>(sp =>
            {
                var correlationAccessor = sp.GetService<ICorrelationContextAccessor>();
                defaultLoggerConfig.GetCorrelationId = () => correlationAccessor?.CorrelationContext?.CorrelationId;
                var logger = new LogglyLogger(defaultLoggerConfig);
                GitHub.Scientist.ResultPublisher = new RefactoringLogger(logger);
                return logger;
            });

            services.AddSingleton<IScientistConfig>(config);

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
               .AddJwtBearer(options =>
               {
                   options.Authority = config.IdentityServiceUrl;
                   options.Audience = "nmbrs";
                   options.RequireHttpsMetadata = true;
               });
            services.AddTenantRequiredPolicy();
            services.AddSystemPolicy();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Computer Service API", Version = "v1" });
                c.AddSecurityDefinition("ApiKey", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "ApiKey"}
                        },
                        new[] { "readAccess", "writeAccess" }
                    }
                });
            });

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                        .AllowAnyMethod()
                        .AllowCredentials()
                        .SetIsOriginAllowed((host) => true)
                        .AllowAnyHeader());
            });

            services.AddControllers();

            services.AddScoped<ILanguageTextService, LanguageTextService>();

            services.AddMediatR(new Assembly[] { typeof(CreateComputerCommandHandler).Assembly });

            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(LoggingBehavior<,>));
            services.AddFluentValidation(new[] { typeof(CreateComputerCommandValidator).Assembly });

            services.AddSingleton<IQueryConnectionInfo, SqlQueryConnectionInfo>();

            services.AddSingleton<IComputerStorage>(config);
            services.AddAutoMapper(typeof(MappingProfile),typeof(CoreMappingsProfile));
            services.AddTransient<IComputerQueries, ComputerQueries>(); 

            services.AddHttpClient("generalHttpClient").WithHeaderPropagation();

            

            

            services.AddDbContext<ComputerDbContext>(options =>
            {
                options.UseSqlServer(config.DbConnectionString,
                    sqlOptions => {
                        sqlOptions.MigrationsAssembly(typeof(ComputerDbContext).GetTypeInfo().Assembly.FullName);
                        sqlOptions.EnableRetryOnFailure(
                            maxRetryCount: 3,
                            maxRetryDelay: TimeSpan.FromSeconds(30), //The delay between retries is randomized up to the specified value.
                            errorNumbersToAdd: null
                            );
                    });
            }, ServiceLifetime.Transient, ServiceLifetime.Transient);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Computer Service API V1"));
            }

            var instanceId = Environment.GetEnvironmentVariable("WEBSITE_INSTANCE_ID");
            var logger = app.ApplicationServices.GetService<ILogger>();
            logger.Information(nameof(Startup), $"{env.ApplicationName} is starting. Environment = {env.EnvironmentName}; InstanceId = {instanceId}; MachineName = {Environment.MachineName}");
            
            if (!Debugger.IsAttached)
            {
                app.UseHealthPing("/health/ping");
                app.UseHealthChecksWithTracking("/health", new HealthCheckOptions
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });
            }

            app.UseGlobalExceptionHandler(x =>
            {
                x.ContentType = "application/json";
                x.ResponseBody(s => JsonConvert.SerializeObject(new
                {
                    Message = "An error occurred whilst processing your request"
                }));
                x.OnError((exception, httpcontext) =>
                {
                    logger.Error(env.ApplicationName, "Internal error", exception: exception);
                    return Task.CompletedTask;
                });
            });

            //Communication.CorrelationId.CorrelationIdExtensions.UseCorrelationId(app);

            app.UseHeaderPropagation();

            if (Configuration.UseAPIKey())
                app.UseAPIKeyMiddleware("/api");
            app.UseRouting();
            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}
