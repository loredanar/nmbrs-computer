using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nmbrs.Computer.Service.Api.Models.DTO;
using Nmbrs.Computer.Tests.Unit.Helpers;
using System;
using Models = Nmbrs.Computer.Service.Core.Queries.Models;


namespace Nmbrs.Computer.Tests.Unit.Queries.Debtor
{
    [TestClass]
    public class TestComputerMappers
    {
        private IMapper _mapper;

        [TestInitialize]
        public void Init()
        {
            _mapper = AutoMapperHelper.GetTestMapper();
        }

        [TestMethod]
        public void Computer_ComputerDTO_Success()
        {
            var Computer = new Models.Computer
            {
                Id = 1,
                UniqueId = "test",
                CompanyId = 1,
                TenantId = Guid.NewGuid(),
                Brand = 1,
                Cpu = 1
            };

            var mapped = _mapper.Map<ComputerDTO>(Computer);

            Assert.AreEqual(Computer.UniqueId, mapped.UniqueId);
            Assert.AreEqual(Computer.CompanyId, mapped.CompanyId);
            Assert.AreEqual(Computer.TenantId, mapped.TenantId);
            Assert.AreEqual(Computer.Brand, mapped.Brand);
            Assert.AreEqual(Computer.Cpu, mapped.Cpu);
        }
    }
}
