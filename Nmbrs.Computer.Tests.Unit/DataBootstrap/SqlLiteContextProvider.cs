using Microsoft.EntityFrameworkCore;
using System;

namespace Nmbrs.Computer.Tests.Unit.DataBootstrap
{
    //TODO move this class to a package
    public class SqlLiteContextProvider<T> where T : DbContext
    {
        public T GetContext()
        {
            var options = new DbContextOptionsBuilder<T>()
                .UseSqlite("DataSource=:memory:")
                .Options;

            var context = (T)Activator.CreateInstance(typeof(T), options);
            context.Database.OpenConnection();
            context.Database.EnsureCreated();

            return context;
        }
    }
}
