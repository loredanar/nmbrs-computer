using Nmbrs.Computer.Service.Data;
//using Nmbrs.Computer.Service.Tests.Unit.DataBootstrap;

namespace Nmbrs.Computer.Tests.Unit
{
    public static class Extensions
    {
        public static void Seed(this ComputerDbContext context)
        {
            //context.Debtors.AddRange(DataProvider.Debtors);

            context.SaveChanges();
        }

        public static bool HasMethod(this object objectToCheck, string methodName)
        {
            var type = objectToCheck.GetType();
            return type.GetMethod(methodName) != null;
        }
    }
}
