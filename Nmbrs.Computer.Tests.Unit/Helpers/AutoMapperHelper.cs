using AutoMapper;
using Nmbrs.Computer.Service.Api.Mappings;
using Nmbrs.Computer.Service.Core.Mappings;

namespace Nmbrs.Computer.Tests.Unit.Helpers
{
    public static class AutoMapperHelper
    {
        /// <summary>
        /// Gets a test IMapper configured with MappingProfile and mocked CorrelationIdResolver and ICorrelationContextAccessor
        /// </summary>
        /// <typeparam name="T">The source member for CorrelationIdResolver</typeparam>
        /// <returns></returns>
        public static IMapper GetTestMapper()
        {
            var configuration = new MapperConfiguration(cfg => {
                cfg.AddProfile(new MappingProfile());
                cfg.AddProfile(new CoreMappingsProfile());
            });
            return new Mapper(configuration);
        }
    }
}