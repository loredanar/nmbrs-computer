using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nmbrs.Computer.Service.Core.Commands;
using Nmbrs.Computer.Service.Data;
using Nmbrs.Computer.Tests.Unit.Helpers;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Nmbrs.Computer.Tests.Unit.Commands.Handlers
{
    [TestClass]
    [TestCategory("Commands")]
    public class CreateComputerCommandHandlerTests
    {
        private ComputerDbContext _context;
        private CreateComputerCommandHandler _handler;
        private IMapper _mapper;
        private CreateComputerCommand _testCommand;

        [TestInitialize]
        public void Init()
        {
            var dbOptionsBuilder = new DbContextOptionsBuilder<ComputerDbContext>()
                                        .UseInMemoryDatabase(databaseName: nameof(CreateComputerCommandHandlerTests));
            _context = new ComputerDbContext(dbOptionsBuilder.Options);

            _mapper = AutoMapperHelper.GetTestMapper();

            _handler = GetHandler();

            _testCommand = new CreateComputerCommand("New Test", 0, Guid.Empty, 0, 0, null, false, 0, 0, 0, 0);
        }

        [TestMethod]
        public async Task CreateComputerCommandHandler_Successful()
        {
           var result = await _handler.Handle(_testCommand, CancellationToken.None);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessful);

            var returnedInfo = (int)result.Value;
            Assert.IsNotNull(returnedInfo);

            var valueDb = await _context.Computer.FirstOrDefaultAsync(x => x.Id == returnedInfo);

            Assert.IsNotNull(valueDb);
            Assert.AreEqual(_testCommand.UniqueId, valueDb.UniqueId);
            Assert.AreEqual(_testCommand.CompanyId, valueDb.CompanyId);
            Assert.AreEqual(_testCommand.TenantId, valueDb.TenantId);
            Assert.AreEqual(_testCommand.Brand, valueDb.Brand);
            Assert.AreEqual(_testCommand.Cpu, valueDb.Cpu);
        }

        private CreateComputerCommandHandler GetHandler()
        {
            return new CreateComputerCommandHandler(_context, _mapper);
        }

    }
}
