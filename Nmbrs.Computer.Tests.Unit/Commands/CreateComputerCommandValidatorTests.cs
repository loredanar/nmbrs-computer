using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nmbrs.Computer.Service.Core.Commands;
using Nmbrs.Computer.Service.Core.Commands.Validators;
using System;
using System.Linq;

namespace Nmbrs.Computer.Tests.Unit.Commands
{
    [TestClass]
    [TestCategory("Commands")]
    public class CreateComputerCommandValidatorTests
    {
        private CreateComputerCommandValidator _validator;

        [TestInitialize]
        public void Init()
        {
            _validator = new CreateComputerCommandValidator();
        }

        [TestMethod]
        public void CreateComputerCommand_EmptyFields_CommandNotValid()
        {
            var command = new CreateComputerCommand(string.Empty, 0, Guid.Empty, 0, 0, null, false, 0, 0, 0, 0);
            var result = _validator.Validate(command);

            Assert.IsFalse(result.IsValid);
            Assert.AreEqual(5, actual: result.Errors.Count());
        }
    }
}