using FluentValidation.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Nmbrs.Computer.Tests.Unit.Commands
{
    public static class ValidationExtensions
    {
        public static void AssertSingleEmptyValidationError(this ValidationResult result, string property)
        {
            Assert.IsFalse(result.IsValid);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.AreEqual($"'{property}' {TestResources.ValidationMessage_NotEmpty}", result.Errors.First().ErrorMessage);
        }

        public static void AssertSingleMaxLengthValidationError(this ValidationResult result, string property, int maxLength, int maxLengthExceeded)
        {
            var expectedMessage = $"The length of '{property}' {string.Format(TestResources.ValidationMessage_MaxLength, maxLength, maxLengthExceeded)}";

            Assert.IsFalse(result.IsValid);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.AreEqual(expectedMessage, result.Errors.First().ErrorMessage);
        }

        public static void AssertValueMustNotBeEqualToNone(this ValidationResult result, string property)
        {
            Assert.IsFalse(result.IsValid);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.AreEqual($"'{property}' {TestResources.ValidationMessage_NotNone}", result.Errors.First().ErrorMessage);

        }

        public static void AssertValueMustNotEqualToEmptyGuid(this ValidationResult result, string property)
        {
            Assert.IsFalse(result.IsValid);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.AreEqual($"'{property}' {TestResources.ValidationMessage_NotEqualToEmptyGuid}", result.Errors.First().ErrorMessage);

        }
    }
}
