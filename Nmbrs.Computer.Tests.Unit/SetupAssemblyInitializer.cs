using Dapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nmbrs.Computer.Tests.Unit.DataBootstrap;

namespace Nmbrs.Computer.Service.Tests.Unit
{
    [TestClass]
    public static class SetupAssemblyInitializer
    {
        [AssemblyInitialize]
        public static void AssemblyInit(TestContext context)
        {
            // This is needed because Dapper does not know how to cast to Guid from byte[], the representation used by Microsoft.EntityFrameworkCore.Sqlite
            // https://github.com/StackExchange/Dapper/issues/718
            SqlMapper.AddTypeHandler(new SqlLiteGuidTypeHandler());
        }
    }
}
