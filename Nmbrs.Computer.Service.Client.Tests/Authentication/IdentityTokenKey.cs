using System;

namespace Nmbrs.Computer.Service.Client.Tests.Authentication
{
    public class IdentityTokenKey
    {
        public Guid TenantId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
