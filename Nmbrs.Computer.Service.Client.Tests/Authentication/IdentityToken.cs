using System;

namespace Nmbrs.Computer.Service.Client.Tests.Authentication
{
    public class IdentityToken
    {
        public IdentityTokenKey Key { get; set; }
        public string Value { get; set; }

        public bool HasKey(Guid tenantId, string username, string password)
        {
            return Key.TenantId == tenantId && Key.Username == username && Key.Password == password;
        }
    }
}
