using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nmbrs.Identity.Client;
using Nmbrs.Identity.Client.Model;
using Nmbrs.Computer.Service.Client.Tests.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nmbrs.Computer.Service.Client.Tests.Authentication
{
    public class IdentityHelper
    {
        private readonly IdentityProvider _identityProvider;
        private readonly ClientCredentialsProvider _clientCredentialsProvider;
        private readonly List<IdentityToken> _tokensCache;

        private string _systemToken;

        public IdentityHelper(IdentityProviderConfig identityConfig, ClientCredentialsProviderConfig clientCredentialsConfig)
        {
            _identityProvider = new IdentityProvider(identityConfig.Endpoint, identityConfig.ClientId, identityConfig.ClientSecret, identityConfig.ApiKey);
            _clientCredentialsProvider = new ClientCredentialsProvider(identityConfig.Endpoint, clientCredentialsConfig.ClientId, clientCredentialsConfig.ClientSecret);
            _tokensCache = new List<IdentityToken>();
        }

        public async Task<string> GetAccessToken(string username, string password, Guid tenantId)
        {
            IdentityToken cachedToken = _tokensCache.FirstOrDefault(t => t.HasKey(tenantId, username, password));
            if (cachedToken != null)
                return cachedToken.Value;

            var result = await _identityProvider.Register(username, password, tenantId);
            if (!result.Success && result.Errors.SingleOrDefault()?.NumericCode != IdentityErrorCodes.DuplicateUserName)
                Assert.Inconclusive("Test could not be run. Process of authentication using Identity failed.");

            string token = await _identityProvider.GetAccessToken(username, password, tenantId);

            _tokensCache.Add(new IdentityToken
            {
                Key = new IdentityTokenKey { TenantId = tenantId, Username = username, Password = password },
                Value = token
            });

            return token;
        }

        public async Task<string> GetClientCredentailsToken()
        {
            if (string.IsNullOrWhiteSpace(_systemToken))
            {
                var result = await _clientCredentialsProvider.GetToken(null);
                _systemToken = result?.accessToken;
            }

            return _systemToken;
        }

        public void DeleteCachedUsers()
        {
            List<Task> tasks = new List<Task>();
            foreach (var token in _tokensCache)
            {
                tasks.Add(_identityProvider.Delete(token.Value, token.Key.Username, token.Key.TenantId));
            }
            Task.WaitAll(tasks.ToArray());
        }
    }
}
