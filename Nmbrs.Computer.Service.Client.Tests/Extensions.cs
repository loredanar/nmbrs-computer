namespace Nmbrs.Computer.Service.Client.Tests
{
    public static class Extensions
    {
        public static bool IsKitchen(this string setting)
        {
            return setting.ToLower() == "kitchen";
        }
    }
}
