using Microsoft.Extensions.Configuration;
using System;

namespace Nmbrs.Computer.Service.Client.Tests.Helpers
{
    public static class ConfigurationHelper
    {
        public static IConfigurationRoot GetConfiguration()
        {
            var envVariable = GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            if (envVariable == null) envVariable = "Development";

            var config = new ConfigurationBuilder()
                        .AddJsonFile("appsettings.json")
                        .AddJsonFile($"appsettings.{envVariable}.json", optional: true)
                        .Build();
            return config;
        }

        public static string GetEnvironmentVariable(string environmentVariableName)
        {
            var environmentVariableValue = Environment.GetEnvironmentVariable(environmentVariableName);

            if (string.IsNullOrEmpty(environmentVariableValue))
            {
                environmentVariableValue = Environment.GetEnvironmentVariable(environmentVariableName, EnvironmentVariableTarget.User);
            }

            if (string.IsNullOrEmpty(environmentVariableValue))
            {
                environmentVariableValue = Environment.GetEnvironmentVariable(environmentVariableName, EnvironmentVariableTarget.Machine);
            }

            Console.WriteLine($"[{environmentVariableName}] => [{environmentVariableValue}]");
            return environmentVariableValue;
        }

        public static string GetConfigValue(this IConfigurationRoot config, string environmentVariableName, string configPath)
        {
            var environmentVariableValue = GetEnvironmentVariable(environmentVariableName);

            if (!string.IsNullOrEmpty(environmentVariableValue))
            {
                return environmentVariableValue;
            }
            return config[configPath];
        }
    }
}
