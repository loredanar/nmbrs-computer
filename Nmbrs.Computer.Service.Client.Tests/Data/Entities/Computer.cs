using System;

namespace Nmbrs.Computer.Service.Client.Tests.Data.Entities
{
    public class Computer
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
