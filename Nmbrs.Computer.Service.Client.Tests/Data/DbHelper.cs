using Dapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nmbrs.Computer.Service.Client.Tests.Models;
using System.Data;
using System.Data.SqlClient;
using Entities = Nmbrs.Computer.Service.Client.Tests.Data.Entities;

namespace Nmbrs.Computer.Service.Client.Tests.Data
{
    internal class DbHelper
    {
        private readonly string _connectionString;
        private readonly string _namePrefix;

        public DbHelper(ComputerSettingsConfig tenantSettingsConfig, TestAppConfig testAppConfig, TestDataConfig testDataConfig)
        {
            _connectionString = tenantSettingsConfig.ConnectionString;
            _namePrefix = testDataConfig.NamePrefix;
        }

        internal Entities.Computer QueryByName(string name)
        {
            using (var connection = CreateConnection())
            {
                return connection.QueryFirstOrDefault<Entities.Computer>($"SELECT * FROM Computer WHERE name = '{name}'");
            }
        }


        internal void CleanupSampleEntities()
        {
            using (var connection = CreateConnection())
            {
                connection.ExecuteScalar($"DELETE s FROM Computer WHERE name like '{_namePrefix}%'");
            }
        }

        internal IDbConnection CreateConnection()
        {
            var connection = new SqlConnection(_connectionString);

            try { connection.Open(); }
            catch { Assert.Inconclusive("Could not connect to the database to validate test."); }

            return connection;
        }
    }
}
