using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Nmbrs.Computer.Service.Client.Tests.Helpers;
using Nmbrs.Computer.Service.Client.Tests.Models;
using Nmbrs.Computer.Service.Client.Tests.Authentication;
using Nmbrs.Computer.Service.Client.Tests.Data;

namespace Nmbrs.Computer.Service.Client.Tests
{
    [TestClass]
    public static class Initializer
    {
        private static IConfigurationRoot _config;
        public static IComputerProvider ComputerProvider { get; private set; }
        public static IdentityHelper IdentityHelper { get; private set; }
        internal static DbHelper DatabaseHelper { get; private set; }
        public static TestAppConfig TestAppConfig  { get; private set; }
        public static ComputerSettingsConfig ComputerSettingsConfig { get; private set; }
        public static ClientCredentialsProviderConfig ClientCredentialsProviderConfig { get; private set; }
        public static IdentityProviderConfig IdentityProviderConfig { get; private set; }
        public static TestDataConfig TestDataConfig { get; private set; }

        [AssemblyInitialize]
        public static void InitializeTests(TestContext context)
        {
            AssemblyInit(context);

            InitGlobal();
        }

        public static void AssemblyInit(TestContext context)
        {
            string core_environment = ConfigurationHelper.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            if (string.IsNullOrEmpty(core_environment))
            {
                using (var file = File.OpenText("Properties\\launchSettings.json"))
                {
                    var reader = new JsonTextReader(file);
                    var jObject = JObject.Load(reader);

                    var variables = jObject
                        .GetValue("profiles")
                        //select a proper profile here
                        .SelectMany(profiles => profiles.Children())
                        .SelectMany(profile => profile.Children<JProperty>())
                        .Where(prop => prop.Name == "environmentVariables")
                        .SelectMany(prop => prop.Value.Children<JProperty>())
                        .ToList();

                    foreach (var variable in variables)
                    {
                        Environment.SetEnvironmentVariable(variable.Name, variable.Value.ToString());
                    }
                }
            }
        }

        private static void InitGlobal()
        {
            InitConfiguration();
            InitComputerProvider();

            IdentityHelper = new IdentityHelper(IdentityProviderConfig, ClientCredentialsProviderConfig);
            DatabaseHelper = new DbHelper(ComputerSettingsConfig, TestAppConfig, TestDataConfig);
        }

        private static void InitConfiguration()
        {
            _config = ConfigurationHelper.GetConfiguration();

            ComputerSettingsConfig = new ComputerSettingsConfig(_config.GetConfigValue("DbConnectionString", "ComputerSettings:Database-connectionString"),
                                                               _config["ComputerSettings:ComputerServiceEndpoint"],
                                                               _config["ComputerSettings:ApiKey"],
                                                               _config["ComputerSettings:TestsKitchen"]);

            TestAppConfig = new TestAppConfig(_config["TestApp:Environment"],
                                                _config["TestApp:Name"],
                                                _config["TestApp:Identifier"],
                                                new RequestUserModel(
                                                _config["TestApp:RequestUser:Id"],
                                                _config["TestApp:RequestUser:UserName"],
                                                _config["TestApp:RequestUser:Password"],
                                                _config["TestApp:RequestUser:TenantId"]));

            ClientCredentialsProviderConfig = new ClientCredentialsProviderConfig(_config["ClientCredentialsProvider:ClientId"],
                                                                                    _config["ClientCredentialsProvider:ClientSecret"]);

            IdentityProviderConfig = new IdentityProviderConfig(_config["IdentityProvider:Endpoint"],
                                                                _config["IdentityProvider:ApiKey"],
                                                                _config["IdentityProvider:ClientId"],
                                                                _config["IdentityProvider:ClientSecret"]);

            TestDataConfig = new TestDataConfig(_config["TestData:namePrefix"]);

        }

        public static void InitComputerProvider()
        {
            ComputerProvider = new ComputerProvider(
                new ComputerProviderConfig(
                    ComputerSettingsConfig.ComputerServiceEndpoint,
                    ComputerSettingsConfig.ApiKey,
                    GetInstance()),
                TestAppConfig.Name,
                TestAppConfig.Environment);
        }

        [AssemblyCleanup]
        public static void Cleanup()
        {
            IdentityHelper.DeleteCachedUsers();
            DatabaseHelper.CleanupSampleEntities();
        }

        /// <summary>
        /// Might be needed when testing with multiple TenantProvider instances, because HttpClient is static. 
        /// Should be called after tests that SetConnectionOptions for TenantProvider with different arguments than default
        /// </summary>
        public static void ResetTenantProviderConnection()
        {
           ComputerProvider.SetConnectionOptions(ComputerSettingsConfig.ComputerServiceEndpoint, ComputerSettingsConfig.ApiKey, GetInstance());
        }

        public static string GetInstance()
        {
            return TestAppConfig.Environment.IsKitchen() ? ComputerSettingsConfig.TestsKitchen : null;
        }
    }
}
