namespace Nmbrs.Computer.Service.Client.Tests.Models
{
    public class ClientCredentialsProviderConfig
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }

        public ClientCredentialsProviderConfig(string clientId, string clientSecret)
        {
            ClientId = clientId;
            ClientSecret = clientSecret;
        }
    }
}
