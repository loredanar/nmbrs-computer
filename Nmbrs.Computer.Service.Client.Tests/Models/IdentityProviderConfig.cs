namespace Nmbrs.Computer.Service.Client.Tests.Models
{
    public class IdentityProviderConfig
    {       
        public string Endpoint { get; set; }
        public string ApiKey { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }

        public IdentityProviderConfig(string endpoint, string apiKey, string clientId, string clientSecret)
        {
            Endpoint = endpoint;           
            ApiKey = apiKey;
            ClientId = clientId;
            ClientSecret = clientSecret;
        }
    }
}
