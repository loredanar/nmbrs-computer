using System;

namespace Nmbrs.Computer.Service.Client.Tests.Models
{
    public class TestAppConfig
    {
        public string Environment { get; set; }
        public string Name { get; set; }
        public Guid Identifier { get; set; }
        public RequestUserModel RequestUser { get; set; }

        public TestAppConfig(string endpoint, string name, string identifier, RequestUserModel requestUser)
        {
            Environment = endpoint;
            Name = name;
            Identifier = new Guid(identifier);
            RequestUser = requestUser;
        }
    }
}
