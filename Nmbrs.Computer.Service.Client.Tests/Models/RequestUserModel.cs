using System;

namespace Nmbrs.Computer.Service.Client.Tests.Models
{
    public class RequestUserModel
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public Guid TenantId { get; set; }

        public RequestUserModel(string id, string username, string password, string tenantId)
        {
            Id = new Guid(id);
            UserName = username;
            Password = password;
            TenantId = new Guid(tenantId);
        }    
    }
}
