namespace Nmbrs.Computer.Service.Client.Tests.Models
{
    public class ComputerSettingsConfig
    {
        public string ConnectionString { get; set; }
        public string ComputerServiceEndpoint { get; set; }
        public string ApiKey { get; set; }
        public string TestsKitchen { get; set; }

        public ComputerSettingsConfig(string connectionString, string endpoint, string apiKey, string testsKitchen)
        {
            ConnectionString = connectionString;
            ComputerServiceEndpoint = endpoint;
            ApiKey = apiKey;
            TestsKitchen = testsKitchen;
        }
    }
}
