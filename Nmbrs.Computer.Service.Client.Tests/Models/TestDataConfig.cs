namespace Nmbrs.Computer.Service.Client.Tests.Models
{
    public class TestDataConfig
    {
        public string NamePrefix { get; set; }

        public TestDataConfig(string namePrefix)
        {
            NamePrefix = namePrefix;
        }
    }
}
