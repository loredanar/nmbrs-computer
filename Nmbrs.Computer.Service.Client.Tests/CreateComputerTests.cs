using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nmbrs.Computer.Service.Client.Model.Request;
using Nmbrs.Computer.Service.Client.Tests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nmbrs.Computer.Service.Client.Tests
{
    [TestClass]
    public class CreateComputerTests
    {
        private CreateComputerRequest _requestModel;
        private string _systemToken;
        private bool _resetTenantConnection;

        [TestInitialize]
        public async Task Init()
        {
            _systemToken = await Initializer.IdentityHelper.GetClientCredentailsToken();

            _requestModel = new CreateComputerRequest(Initializer.TestAppConfig.RequestUser.Id, _systemToken);
        }

        [TestMethod]
        public async Task Create_Valid_Success()
        {
            var result = await Initializer.ComputerProvider.Create(_requestModel);

            Assert.IsTrue(result.IsSuccessful);
            Assert.IsNotNull(result.Errors);
            Assert.AreEqual(0, result.Errors.Count());
            Assert.IsNotNull(result.Content);
            Assert.AreNotEqual(Guid.Empty, result.Content.Id);

            //var value = Initializer.DatabaseHelper.QueryByName(_requestModel.Name);
            //Assert.IsNotNull(value);
            //Assert.AreEqual(result.Content.Id, value.Id);
        }
    }
}
