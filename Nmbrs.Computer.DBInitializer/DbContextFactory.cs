using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Nmbrs.Computer.Service.Data;
using Nmbrs.Tests.Core;
using System;

namespace Nmbrs.Computer.DBInitializer
{
    public class DbContextFactory : IDesignTimeDbContextFactory<ComputerDbContext>
    { 
        private static IConfigurationRoot Configuration { get; set; }

        private static ComputerVaultSecrets GetSecrets()
        {
            Console.WriteLine(ConfigurationHelper.GetEnvironment());

            Configuration = ConfigurationHelper.GetConfiguration();
            //if (ConfigurationHelper.GetEnvironment().ToUpperInvariant().Contains("DEVELOPMENT"))
            //{
            return Configuration.GetSection("TemplateSecrets").Get<ComputerVaultSecrets>();
            //}
            //else
            //{
            //    throw new NotImplementedException();
            //var keyVault = new VaultHelper(Configuration, ConfigurationHelper.GetEnvironment());
            //return keyVault.GetConfiguration<AuthorizationVaultSecrets>().GetAwaiter().GetResult();
            //}
        }

        public ComputerDbContext CreateDbContext(string[] args)
        {
            var secrets = GetSecrets();
            var connectionstring = secrets.DbConnectionString;

            Console.WriteLine($"DbConnectionString {connectionstring}");

            var dbOptionsBuilder = new DbContextOptionsBuilder<ComputerDbContext>();
                //.UseSqlServer(connectionstring);
            ComputerDbContext.AddBaseOptions(dbOptionsBuilder, connectionstring);

            return new ComputerDbContext(dbOptionsBuilder.Options);
        }
    }
}
