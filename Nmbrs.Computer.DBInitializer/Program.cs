using System;

namespace Nmbrs.Computer.DBInitializer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello! Running ComputerServiceContext context");

            var connectionstring = ConfigHelper.GetEnvironmentVariable("DbConnectionString");
            var dbFactory = new DbContextFactory();
            dbFactory.CreateDbContext(args);
            Console.WriteLine($"DbConnectionString {connectionstring}");
            Console.WriteLine($"Context initialized! Done");
        }

    }
}
