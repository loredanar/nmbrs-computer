using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace Nmbrs.Computer.DBInitializer
{
    public static class ConfigHelper
    {
        public static string GetEnvironmentVariable(string environmentVariableName)
        {
            var environmentVariableValue = Environment.GetEnvironmentVariable(environmentVariableName);

            if (string.IsNullOrEmpty(environmentVariableValue))
            {
                environmentVariableValue = Environment.GetEnvironmentVariable(environmentVariableName, EnvironmentVariableTarget.User);
            }

            if (string.IsNullOrEmpty(environmentVariableValue))
            {
                environmentVariableValue = Environment.GetEnvironmentVariable(environmentVariableName, EnvironmentVariableTarget.Machine);
            }

            if (string.IsNullOrEmpty(environmentVariableValue))
            {
                environmentVariableValue = GetFromJsonFile();
            }

            Console.WriteLine($"[{environmentVariableName}] => [{environmentVariableValue}]");
            return environmentVariableValue;
        }

        public static string GetFromJsonFile()
        {
            var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.Development.json", optional: true, reloadOnChange: true);

            IConfigurationRoot configuration = builder.Build();

            return configuration["ConnectionStrings:DbConnectionString"];
        }
    }
}
