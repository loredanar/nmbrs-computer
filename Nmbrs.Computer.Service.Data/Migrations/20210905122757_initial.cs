﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Nmbrs.Computer.Service.Data.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Computer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UniqueId = table.Column<string>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false),
                    TenantId = table.Column<Guid>(nullable: false),
                    Brand = table.Column<int>(nullable: false),
                    Cpu = table.Column<int>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: true),
                    IsEnabled = table.Column<bool>(nullable: false),
                    StartPeriod = table.Column<int>(nullable: false),
                    StartYear = table.Column<int>(nullable: false),
                    EndPeriod = table.Column<int>(nullable: false),
                    EndYear = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Computer", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Computer");
        }
    }
}
