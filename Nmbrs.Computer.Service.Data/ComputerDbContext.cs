using Microsoft.EntityFrameworkCore;
using Nmbrs.Computer.Service.Data.Mappings;
using System;

namespace Nmbrs.Computer.Service.Data
{
    public partial class ComputerDbContext : DbContext
    {
        public virtual DbSet<Entities.Computer> Computer { get; set; }

        public ComputerDbContext(DbContextOptions<ComputerDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new ComputerMappings());
        }

        public static void AddBaseOptions(DbContextOptionsBuilder<ComputerDbContext> builder, string connectionString)
        {
            if (builder == null)
                throw new ArgumentNullException(nameof(builder));

            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentException("Connection string must be provided", nameof(connectionString));

            builder.UseSqlServer(connectionString, x =>
            {
                x.EnableRetryOnFailure();
            });
        }
    }
}
