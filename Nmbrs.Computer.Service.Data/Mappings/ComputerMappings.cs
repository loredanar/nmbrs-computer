using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Nmbrs.Computer.Service.Data.Mappings
{
    public class ComputerMappings : IEntityTypeConfiguration<Entities.Computer>
    {
        public void Configure(EntityTypeBuilder<Entities.Computer> builder)
        {
            builder.HasKey(t => t.Id);
            builder.Property(t => t.UniqueId).IsRequired();
            builder.Property(t => t.TenantId).IsRequired();
            builder.Property(t => t.CompanyId).IsRequired();
            builder.Property(t => t.Brand).IsRequired();
            builder.Property(t => t.Cpu).IsRequired();
            builder.Property(t => t.EmployeeId);
            builder.Property(t => t.IsEnabled).IsRequired();
            builder.Property(t => t.StartPeriod).IsRequired();
            builder.Property(t => t.StartYear).IsRequired();
            builder.Property(t => t.EndPeriod).IsRequired();
            builder.Property(t => t.EndYear).IsRequired();
        }
    }
}
