using Nmbrs.Computer.Service.Core.Services.Interfaces;

namespace Nmbrs.Tenant.Service.Tests.Integration.Helpers
{

    public class ComputerStorageTests : IComputerStorage
    {
        public string DbConnectionString { get; set; }

        public ComputerStorageTests(string connectionString)
        {
            DbConnectionString = connectionString;
        }
    }
}
