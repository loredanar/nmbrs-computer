using AutoMapper;
using Nmbrs.Computer.Service.Api.Mappings;

namespace Nmbrs.Computer.Service.Tests.Integration.Helpers
{
    public static class AutoMapperHelper
    {
        public static IMapper GetTestMapper()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });

            return new Mapper(configuration);
        }
    }
}
