using Bogus;
using Nmbrs.Infrastructure.EnumsAndConstants.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nmbrs.Computer.Service.Tests.Integration.TestCases
{
    public class TestCaseFaker
    {
        public const string DEFAULT_LOCALE = "en";
        public const string RULESET_DEFAULT = "default";
        public Dictionary<string, Faker<Data.Entities.Computer>> ComputerFakers { get; set; }

        public TestCaseFaker(IEnumerable<string> locales = null)
        {
       
            var ComputerFakers = new Dictionary<string, Faker<Data.Entities.Computer>>();

            if (locales == null || !locales.Any())
            {
                locales = new List<string> { DEFAULT_LOCALE };
            }

            foreach (var locale in locales)
            {
                ComputerFakers.Add(locale, GetComputerFaker(locale));
            }
            ComputerFakers = ComputerFakers;
        }


        private static Faker<Data.Entities.Computer> GetComputerFaker(string locale = DEFAULT_LOCALE)
        {
            return new Faker<Data.Entities.Computer>(locale)
                                        .RuleFor(x => x.UniqueId, x => x.Company.Random.Word())
                                        .RuleFor(x => x.CompanyId, x => x.Company.Random.Int())
                                        .RuleFor(x => x.TenantId, x => x.Company.Random.Guid())
                                        .RuleFor(x => x.Brand, x => x.Company.Random.Int())
                                        .RuleFor(x => x.Cpu, x => x.Company.Random.Int());
        }

        public static int GetLanguageByLocale(string locale)
        {
            if (string.IsNullOrEmpty(locale)) return (int)Language.EN;

            object? enumValue;
            if (Enum.TryParse(typeof(Language), locale, true, out enumValue))
            {
                return (int)(Language)enumValue;
            }
            
            return (int)Language.EN;
        }
    }
}
