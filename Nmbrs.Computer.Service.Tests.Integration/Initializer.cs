using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nmbrs.Infrastructure.Data.AzureTable;
using Nmbrs.Infrastructure.Data.AzureTable.Definitions;
using Nmbrs.Computer.Service.Core.Services;
using Nmbrs.Computer.Service.Data;
using Nmbrs.Computer.Service.Tests.Integration.Helpers;
using Nmbrs.Computer.Service.Tests.Integration.TestCases;
using Nmbrs.Tenant.Service.Tests.Integration.Helpers;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Nmbrs.Computer.Service.Tests.Integration
{
    [TestClass]
    public static class Initializer
    {
        private static IConfigurationRoot _config;

        //if NoData remove this
        public static ComputerDbContext ComputerServiceContext { get; private set; }
        public static Guid TestTenantId { get; private set; }
        public static SqlQueryConnectionInfo SqlQueryConnectionInfo { get; private set; }
        public static ComputerStorageTests ComputerStorageTests { get; private set; }
        //---

        public static IMapper Mapper { get; private set; }
        public static TestCaseFaker TestCaseFaker { get; private set; }

        //if AzureTable remove this
        public static ITableProvider<Core.Services.AzureTable.Entities.Computer> ComputerAzureTableProvider { get; private set; }

        [AssemblyInitialize]
        public static async Task InitializeTests(TestContext context)
        {
            AssemblyInit(context);

            await InitGlobal();
        }

        public static void AssemblyInit(TestContext context)
        {
            string core_environment = ConfigurationHelper.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            if (string.IsNullOrEmpty(core_environment))
            {
                using (var file = File.OpenText("Properties\\launchSettings.json"))
                {
                    var reader = new JsonTextReader(file);
                    var jObject = JObject.Load(reader);

                    var variables = jObject
                        .GetValue("profiles")
                        //select a proper profile here
                        .SelectMany(profiles => profiles.Children())
                        .SelectMany(profile => profile.Children<JProperty>())
                        .Where(prop => prop.Name == "environmentVariables")
                        .SelectMany(prop => prop.Value.Children<JProperty>())
                        .ToList();

                    foreach (var variable in variables)
                    {
                        Environment.SetEnvironmentVariable(variable.Name, variable.Value.ToString());
                    }
                }
            }
        }

        private static async Task InitGlobal()
        {
            _config = ConfigurationHelper.GetConfiguration();

            //if NoData remove this
            var databaseConnectionString = _config.GetConfigValue("DbConnectionString", "ComputerSettings:Database-connectionString");
            var dbOptionsBuilder = new DbContextOptionsBuilder<ComputerDbContext>()
                                        .UseSqlServer(databaseConnectionString);

            ComputerServiceContext = new ComputerDbContext(dbOptionsBuilder.Options);
            ComputerStorageTests = new ComputerStorageTests(ComputerServiceContext.Database.GetDbConnection().ConnectionString);
            SqlQueryConnectionInfo = new SqlQueryConnectionInfo(ComputerStorageTests);
            //---

            //if AzureTables remove this
            var providerConfig = new AzureTableProviderConfig
            {
                AccountName = _config.GetValue<string>("AzureProviderConfig:UserName"),
                Password = _config.GetValue<string>("AzureProviderConfig:Password"),
                UseHttps = true
            };
            ComputerAzureTableProvider = new AzureTableProvider<Core.Services.AzureTable.Entities.Computer>(providerConfig);
            //---

            Mapper = AutoMapperHelper.GetTestMapper();

            TestCaseFaker = new TestCaseFaker();

        }

        [AssemblyCleanup]
        public async static Task Cleanup()
        {
            //cleanup all test cases by tenantId
            //ComputerServiceContext.Value.RemoveRange(ComputerServiceContext.Value.Where(x => x.TenantId = TestTenantId));
          
            //await ComputerServiceContext.SaveChangesAsync();
        }
    }
}
