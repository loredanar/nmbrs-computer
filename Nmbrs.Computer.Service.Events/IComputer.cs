using MassTransit;
using System;

namespace Nmbrs.Computer.Service.Events
{
    public interface IComputer : CorrelatedBy<int>
    {
        int Id { get; set; }
        DateTimeOffset Timestamp { get; }
    }
}
