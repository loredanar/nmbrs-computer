using System;
using System.Collections.Generic;

namespace Nmbrs.Computer.Service.Core
{
  [AttributeUsage(AttributeTargets.Parameter)]
  public sealed class ValidatedNotNullAttribute : Attribute { }

  public static class Guard
  {
    public static void NotNull<T>([ValidatedNotNull] this T value, string name) where T : class
    {
      if (value == null)
        throw new ArgumentNullException(name);
    }

    public static void NotEmpty([ValidatedNotNull] Guid value, string name)
    {
      if (value == Guid.Empty)
        throw new ArgumentNullException(name);
    }

    public static void NotEmpty([ValidatedNotNull] string value, string name)
    {
      if (string.IsNullOrEmpty(value))
        throw new ArgumentNullException(name);
    }

    public static void NotEmpty(IEnumerable<string> items, string name)
    {
      NotNull(items, name);
      foreach (var item in items)
      {
        NotEmpty(item, name);
      }
    }
  }
}
