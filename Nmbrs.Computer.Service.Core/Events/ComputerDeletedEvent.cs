using Nmbrs.Computer.Service.Events;
using System;

namespace Nmbrs.Computer.Service.Core.Events
{
    public class ComputerDeletedEvent : IComputerDeleted
    {
        public int Id { get; set; }

        public DateTimeOffset Timestamp { get; set; }

        public int CorrelationId { get; set; }
    }
}
