using Nmbrs.Computer.Service.Events;
using System;

namespace Nmbrs.Computer.Service.Core.Events
{
    public class ComputerCreatedEvent : IComputerCreated
    {
        public int Id { get; set; }
        public string UniqueId { get; set; }
        public int CompanyId { get; set; }
        public Guid TenantId { get; set; }
        public int Brand { get; set; }
        public int Cpu { get; set; }
        public int? EmployeeId { get; set; }
        public bool IsEnabled { get; set; }
        public int StartPeriod { get; set; }
        public int StartYear { get; set; }
        public int EndPeriod { get; set; }
        public int EndYear { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public int CorrelationId { get; set; }
    }
}
