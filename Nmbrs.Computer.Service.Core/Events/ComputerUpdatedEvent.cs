using Nmbrs.Computer.Service.Events;
using System;

namespace Nmbrs.Computer.Service.Core.Events
{
    public class ComputerUpdatedEvent : IComputerUpdated
    {
        public int Id { get; set; }

        public DateTimeOffset Timestamp { get; set; }

        public int CorrelationId { get; set; }
    }
}
