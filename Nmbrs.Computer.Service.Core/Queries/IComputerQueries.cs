using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nmbrs.Computer.Service.Core.Queries
{
    public interface IComputerQueries
    {
        Task<IReadOnlyCollection<Models.Computer>> GetAll(Guid tenantId, string instance = null);
        Task<Models.Computer> GetByTenantIdAndComputerId(Guid tenantId, int id);
    }
}
