using Dapper;
using Nmbrs.Computer.Service.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Nmbrs.Computer.Service.Core.Queries
{
    public class ComputerQueries : IComputerQueries
    {
        private readonly IQueryConnectionInfo _connectionInfo;

        public ComputerQueries(IQueryConnectionInfo connectionInfo)
        {
            _connectionInfo = connectionInfo;
        }

        public async Task<IReadOnlyCollection<Models.Computer>> GetAll(Guid tenantId, string instance = null)
        {
            using (var connection = _connectionInfo.GetDbConnection())
            {
                var results = await connection.QueryAsync<Models.Computer>(QueriesResources.GetAllByTenantId, new { Instance = instance, TenantId = tenantId });

                return new ReadOnlyCollection<Models.Computer>(results.ToList());
            }
        }

        public async Task<Models.Computer> GetByTenantIdAndComputerId(Guid tenantId, int id)
        {
            if (tenantId == Guid.Empty)
                throw new ArgumentNullException(nameof(tenantId));
            if (id == 0)
                throw new ArgumentNullException(nameof(id));

            using (var connection = _connectionInfo.GetDbConnection())
            {
                return await connection.QueryFirstOrDefaultAsync<Models.Computer>(QueriesResources.GetByTenantIdAndComputerId, new { TenantId = tenantId, Id = id });
            }
        }
    }
}
