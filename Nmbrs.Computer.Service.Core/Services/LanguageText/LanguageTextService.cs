using Microsoft.Extensions.Caching.Memory;
using Nmbrs.Infrastructure.EnumsAndConstants.Enums;
using Nmbrs.Infrastructure.Log.Interfaces;
using Nmbrs.LanguageText.Client;
using Nmbrs.LanguageText.Client.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Nmbrs.Computer.Service.Core.Services.LanguageText
{
    public class LanguageTextService : ILanguageTextService
    {
        private const string _cacheKey = "LanguageText_";
        private readonly ILanguageTextSettings _languageTextSettings;
        private readonly ILanguageTextClient _client;
        private readonly IMemoryCache _memoryCache;
        private readonly ILogger _logger;

        public LanguageTextService(ILanguageTextSettings languageTextSettings, IMemoryCache memoryCache, ILogger logger)
        {
            _languageTextSettings = languageTextSettings;
            _memoryCache = memoryCache;
            _client = new LanguageTextClient(languageTextSettings.LanguageTextUserName, languageTextSettings.LanguageTextPassword, 
                                                languageTextSettings.ServiceUri, languageTextSettings.LanguageTextApiKey);
            _logger = logger;
        }

        public async Task<IEnumerable<ILanguageTextModel>> Get(Language? language = null)
        {
            Dictionary<string, object> embeddedObjects = LanguageTextInformation();

            try
            {
                _logger.Information(CoreResources.ApplicationName, $"Start getting language texts", embeddedObjects: embeddedObjects);

                var texts = await _memoryCache.GetOrCreate(_cacheKey, entry =>
                {
                    entry.SlidingExpiration = TimeSpan.FromHours(1);
                    return _client.GetAsync(_languageTextSettings.AppName);
                });

                if (texts == null)
                {
                    _logger.Warning(CoreResources.ApplicationName, $"No language texts returned", embeddedObjects: embeddedObjects);
                    return new Collection<ILanguageTextModel>();
                }

                if (language.HasValue)
                {
                    texts = texts.Where(x => x.Lang == language.Value.ToString().ToLower());
                }

                return texts.Select(t => new LanguageTextModel(t, _languageTextSettings.Prefix));
            }
            catch (Exception exception)
            {
                ExceptionHandling(embeddedObjects, exception, "Error when getting language text values");
                return new Collection<ILanguageTextModel>();
            }
        }

        public async Task<string> GetByKey(string resourceKey, Language language)
        {
            var embeddedObjects = LanguageTextInformation();
            embeddedObjects.Add("Key", resourceKey);
            try
            {
                _logger.Information(CoreResources.ApplicationName, $"Start getting language text by key", embeddedObjects: embeddedObjects);

                var texts = await _memoryCache.GetOrCreate(_cacheKey, entry =>
                {
                    entry.SlidingExpiration = TimeSpan.FromHours(1);
                    return _client.GetAsync(_languageTextSettings.AppName);
                });

                if (texts == null)
                {
                    _logger.Warning(CoreResources.ApplicationName, $"No language texts returned", embeddedObjects: embeddedObjects);
                    return resourceKey;
                }

                var text = texts.FirstOrDefault(x => x.Key == resourceKey && x.Lang == language.ToString().ToLower()).Text;

                if (!string.IsNullOrEmpty(text)) return text;

                return resourceKey;

            }
            catch (Exception exception)
            {
                ExceptionHandling(embeddedObjects, exception, "Error when getting language text by key");
                return string.Empty;
            }
        }

        public async Task<string> GetEnumDisplayName<T>(T enumValue, Language language) where T : Enum
        {
            var embeddedObjects = LanguageTextInformation();

            _logger.Information(CoreResources.ApplicationName, $"Start getting language text enum value {enumValue}", embeddedObjects: embeddedObjects);

            var key = $"{enumValue.GetType().Name.ToLower()}_{Convert.ToInt32(enumValue)}";

            try
            {
                var texts = await _memoryCache.GetOrCreate(_cacheKey, entry =>
                {
                    entry.SlidingExpiration = TimeSpan.FromHours(1);
                    return _client.GetAsync(_languageTextSettings.AppName);
                });

                if (texts == null)
                {
                    _logger.Warning(CoreResources.ApplicationName, $"No language texts returned", embeddedObjects: embeddedObjects);
                    return enumValue.ToString();
                }

                return texts.FirstOrDefault(x => x.Key == key && x.Lang == language.ToString().ToLower()).Text;

            }
            catch (Exception exception)
            {
                ExceptionHandling(embeddedObjects, exception, "Error when getting language text enum values");
                return enumValue.ToString();
            }
        }

        private void ExceptionHandling(Dictionary<string, object> embeddedObjects, Exception exception, string message)
        {
            _logger.Error(CoreResources.ApplicationName, message, exception, embeddedObjects: embeddedObjects);
            _memoryCache.Remove(_cacheKey);
        }

        private Dictionary<string, object> LanguageTextInformation()
        {
            return new Dictionary<string, object>
            {
                { "LanguageText_ServiceUri", _languageTextSettings?.ServiceUri },
                { "LanguageText_AppName", _languageTextSettings?.AppName },
                { "LanguageText_Prefix", _languageTextSettings?.Prefix }
            };
        }
    }
}
