using System;

namespace Nmbrs.Computer.Service.Core.Services.LanguageText
{
    public class LanguageTextModel : ILanguageTextModel
    {
        private string _key { get; set; }
        private string _text { get; set; }
        private string _prefix { get; set; }
        private string _language { get; set; }
        public string text => _text;
        public string key => _key;
        public string language => _language;

        public LanguageTextModel(Nmbrs.LanguageText.Client.LanguageText languageText, string prefix)
        {
            if (languageText == null)
                throw new ArgumentNullException(nameof(languageText));

            _prefix = prefix;
            _key = _prefix + languageText.Key;
            _text = languageText.Text;
            _language = languageText.Lang;
        }
    }
}
