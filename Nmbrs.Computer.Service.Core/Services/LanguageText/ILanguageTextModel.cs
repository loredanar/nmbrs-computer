namespace Nmbrs.Computer.Service.Core.Services.LanguageText
{
    public interface ILanguageTextModel
    {
        string text { get; }
        string key { get; }
        string language { get; }
    }
}
