using Nmbrs.Infrastructure.EnumsAndConstants.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nmbrs.Computer.Service.Core.Services.LanguageText
{
    public interface ILanguageTextService
    {
        Task<IEnumerable<ILanguageTextModel>> Get(Language? language = null);
        Task<string> GetByKey(string resourceKey, Language language);
        Task<string> GetEnumDisplayName<T>(T enumValue, Language language) where T : Enum;
    }
}
