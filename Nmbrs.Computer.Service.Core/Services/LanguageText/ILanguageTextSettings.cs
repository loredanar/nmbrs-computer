namespace Nmbrs.Computer.Service.Core.Services.LanguageText
{
    public interface ILanguageTextSettings
    {
        string AppName { get; set; }
        string FakeJsonFilePath { get; set; }
        string Prefix { get; set; }
        string ServiceUri { get; set; }
        string LanguageTextUserName { get; set; }
        string LanguageTextPassword { get; set; }
        string LanguageTextApiKey { get; set; }
    }
}
