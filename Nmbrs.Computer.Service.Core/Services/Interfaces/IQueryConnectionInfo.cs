using System.Data;

namespace Nmbrs.Computer.Service.Core.Services.Interfaces
{
    public interface IQueryConnectionInfo
    {
        IDbConnection GetDbConnection();
    }
}
