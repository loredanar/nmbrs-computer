using FluentValidation.Results;
using System.Collections.Generic;

namespace Nmbrs.Computer.Service.Core.Services.Interfaces
{
    public interface IValidationService
    {
        IEnumerable<string> Validate<T>(T entity);
        ValidationResult ValidateCommand(dynamic command);
    }
}
