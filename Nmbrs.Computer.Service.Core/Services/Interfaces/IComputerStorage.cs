namespace Nmbrs.Computer.Service.Core.Services.Interfaces
{
    public interface IComputerStorage
    {
        string DbConnectionString { get; set; }
    }
}
