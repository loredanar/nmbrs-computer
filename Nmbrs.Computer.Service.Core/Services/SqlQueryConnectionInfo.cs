using Nmbrs.Computer.Service.Core.Services.Interfaces;
using System.Data;
using System.Data.SqlClient;

namespace Nmbrs.Computer.Service.Core.Services
{
    public class SqlQueryConnectionInfo : IQueryConnectionInfo
    {
        private readonly IComputerStorage _storage;

        public SqlQueryConnectionInfo(IComputerStorage storage)
        {
            _storage = storage;
        }

        public IDbConnection GetDbConnection() => new SqlConnection(_storage.DbConnectionString);
    }
}
