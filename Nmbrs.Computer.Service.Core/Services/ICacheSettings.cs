using System;

namespace Nmbrs.Computer.Service.Core.Services.Interfaces
{
    public interface ICacheSettings
    {
        uint CacheAbsoluteExpirationInSeconds { get; set; }
        uint CacheSlidingExpirationInSeconds { get; set; }

        DateTimeOffset? GetAbsoluteExpiration();

        TimeSpan? GetSlidingExpiration();
    }
}
