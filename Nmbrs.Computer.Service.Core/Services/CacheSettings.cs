using Nmbrs.Computer.Service.Core.Services.Interfaces;
using System;

namespace Nmbrs.Computer.Service.Core.Services
{
    public class CacheSettings : ICacheSettings
    {
        private const uint DEFAULT_ABSOLUTE_VALUE = 86400;
        private const uint DEFAULT_SLIDING_VALUE = 7200;
        private uint absoluteValue = DEFAULT_ABSOLUTE_VALUE;
        private uint slidingValue = DEFAULT_SLIDING_VALUE;

        public uint CacheAbsoluteExpirationInSeconds
        {
            get
            {
                return absoluteValue;
            }
            set
            {
                if (value <= 0.0) absoluteValue = DEFAULT_ABSOLUTE_VALUE;
                else absoluteValue = value;
            }
        }
        public uint CacheSlidingExpirationInSeconds
        {
            get
            {
                return slidingValue;
            }
            set
            {
                if (value <= 0.0) slidingValue = DEFAULT_SLIDING_VALUE;
                else slidingValue = value;
            }
        }

        public CacheSettings() { }

        public CacheSettings(string absoluteValue, string slidingValue)
        {
            uint.TryParse(absoluteValue, out uint absolute);
            uint.TryParse(slidingValue, out uint sliding);

            CacheAbsoluteExpirationInSeconds = absolute;
            CacheSlidingExpirationInSeconds = sliding;
        }

        public DateTimeOffset? GetAbsoluteExpiration()
        {
            return DateTimeOffset.Now.AddSeconds(CacheAbsoluteExpirationInSeconds);
        }

        public TimeSpan? GetSlidingExpiration()
        {
            return TimeSpan.FromSeconds(CacheSlidingExpirationInSeconds);
        }
    }
}
