using Autofac;
using FluentValidation;
using System;

namespace Nmbrs.Computer.Service.Core.Services
{
    public class AutofacValidatorFactory : ValidatorFactoryBase
    {
        readonly ILifetimeScope _scope;

        public AutofacValidatorFactory(ILifetimeScope scope)
        {
            _scope = scope;
        }

        public override IValidator CreateInstance(Type validatorType)
        {
            return _scope.Resolve(validatorType) as IValidator;
        }
    }
}
