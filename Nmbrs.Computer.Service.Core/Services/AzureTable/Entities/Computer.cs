using Nmbrs.Infrastructure.Data.AzureTable.Attributes;
using Nmbrs.Infrastructure.Data.AzureTable.Definitions;
using System;

namespace Nmbrs.Computer.Service.Core.Services.AzureTable.Entities
{
    public class Computer : StorageTableEntityBase
    {
        [RowKeyProperty]
        public int Id { get; set; }

        [PartitionProperty(Order = 0)]
        public Guid TenantId { get; set; }

        [PartitionProperty(Order = 1)]
        public string UniqueId { get; set; }

        [PartitionProperty(Order = 2)]
        public int CompanyId { get; set; }

        [PartitionProperty(Order = 3)]
        public int Brand { get; set; }

        [PartitionProperty(Order = 4)]
        public int Cpu { get; set; }

        [PartitionProperty(Order = 5)]
        public int? EmployeeId { get; set; }
        
        [PartitionProperty(Order = 6)]
        public bool IsEnabled { get; set; }

        [PartitionProperty(Order = 7)]
        public int StartPeriod { get; set; }

        [PartitionProperty(Order = 8)]
        public int StartYear { get; set; }

        [PartitionProperty(Order = 9)]
        public int EndPeriod { get; set; }

        [PartitionProperty(Order = 10)]
        public int EndYear { get; set; }
    }
}
