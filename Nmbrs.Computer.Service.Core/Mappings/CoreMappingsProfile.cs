using AutoMapper;
using Nmbrs.Computer.Service.Core.Events;
using Nmbrs.Computer.Service.Core.Commands;

namespace Nmbrs.Computer.Service.Core.Mappings
{
    public class CoreMappingsProfile : Profile
    {
        public CoreMappingsProfile()
        {
            CreateMap<CreateComputerCommand, Services.AzureTable.Entities.Computer>()
                .ReverseMap();

            CreateMap<CreateComputerCommand, Data.Entities.Computer>()
                .ReverseMap();

            CreateMap<Data.Entities.Computer, ComputerCreatedEvent>()
                .ReverseMap();
        }
    }
}

