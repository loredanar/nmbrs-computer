using AutoMapper;
using MediatR;
using Nmbrs.Commands.Core.Result;
using Nmbrs.Computer.Service.Data;
using System;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Entities = Nmbrs.Computer.Service.Data.Entities;

[assembly: InternalsVisibleTo("Nmbrs.Computer.Service.Tests.Unit")]

namespace Nmbrs.Computer.Service.Core.Commands
{
    public class CreateComputerCommandHandler : IRequestHandler<CreateComputerCommand, CommandResult>
    {
        private readonly ComputerDbContext _dbContext;
        private readonly IMapper _mapper;

        public CreateComputerCommandHandler(ComputerDbContext dbContext, IMapper mapper/*, IBus bus*//*Uncomment if using messaging*/)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<CommandResult> Handle(CreateComputerCommand request, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var value = _mapper.Map<CreateComputerCommand, Entities.Computer>(request);
             
                await _dbContext.Computer.AddAsync(value);
                await _dbContext.SaveChangesAsync();

                return CommandResult.Ok(value.Id);
            }
            catch (Exception ex)
            {
                return CommandResult.FromException(ex);
            }
        }
    }
}
