using AutoMapper;
using MediatR;
using Nmbrs.Commands.Core.Result;
using Nmbrs.Computer.Service.Data;
using System;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo("Nmbrs.Computer.Service.Tests.Unit")]

namespace Nmbrs.Computer.Service.Core.Commands
{
    public class DeleteComputerCommandHandler : IRequestHandler<DeleteComputerCommand, CommandResult>
    {
        private readonly ComputerDbContext _dbContext;

        public DeleteComputerCommandHandler(ComputerDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<CommandResult> Handle(DeleteComputerCommand request, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var value = _dbContext.Computer.SingleOrDefault(x => x.Id == request.Id);
                if (value == null)
                    return new CommandResult(HttpStatusCode.NotFound, $"Entity not found! Id={nameof(request.Id)}");

                _dbContext.Computer.Remove(value);
                await _dbContext.SaveChangesAsync();

                return CommandResult.Ok(value.Id);
            }
            catch (Exception ex)
            {
                return CommandResult.FromException(ex);
            }
        }
    }
}
