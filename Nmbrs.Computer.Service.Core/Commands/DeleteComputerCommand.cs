using Nmbrs.Commands.Core.Command;

namespace Nmbrs.Computer.Service.Core.Commands
{
    public class DeleteComputerCommand: ICommand
    {
       public int Id { get; private set; }

        public DeleteComputerCommand(int id)
        {
            Id = id;
        }
    }
}
