using Nmbrs.Commands.Core.Command;
using System;

namespace Nmbrs.Computer.Service.Core.Commands
{
    public class UpdateComputerCommand : ICommand
    {
        public int Id { get; private set; }
        public string UniqueId { get; private set; }
        public int CompanyId { get; private set; }
        public Guid TenantId { get; private set; }
        public int Brand { get; private set; }
        public int Cpu { get; private set; }
        public int? EmployeeId { get; private set; }
        public bool IsEnabled { get; private set; }
        public int StartPeriod { get; private set; }
        public int StartYear { get; private set; }
        public int EndPeriod { get; private set; }
        public int EndYear { get; private set; }

        public UpdateComputerCommand(
            int id,
            string uniqueId,
            int companyId,
            Guid tenantId,
            int brand,
            int cpu,
            int? employeeId,
            bool isEnabled,
            int startPeriod,
            int startYear,
            int endPeriod,
            int endYear)
        {
            Id = id;
            UniqueId = uniqueId;
            CompanyId = companyId;
            TenantId = tenantId;
            Brand = brand;
            Cpu = cpu;
            EmployeeId = employeeId;
            IsEnabled = isEnabled;
            StartPeriod = startPeriod;
            StartYear = startYear;
            EndPeriod = endPeriod;
            EndYear = endYear;
        }
    }
}
