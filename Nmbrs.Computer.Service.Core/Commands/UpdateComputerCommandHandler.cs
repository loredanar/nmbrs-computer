using MediatR;
using Nmbrs.Commands.Core.Result;
using Nmbrs.Computer.Service.Data;
using System;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo("Nmbrs.Computer.Service.Tests.Unit")]

namespace Nmbrs.Computer.Service.Core.Commands
{
    public class UpdateComputerCommandHandler : IRequestHandler<UpdateComputerCommand, CommandResult>
    {
        private readonly ComputerDbContext _dbContext;

        public UpdateComputerCommandHandler(ComputerDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<CommandResult> Handle(UpdateComputerCommand request, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {

                var value = _dbContext.Computer.SingleOrDefault(x => x.Id == request.Id);
                if (value == null)
                    return new CommandResult(HttpStatusCode.NotFound, $"Value Entity not found! Id={nameof(request.Id)}");

                value.UniqueId = request.UniqueId;
                value.CompanyId = request.CompanyId;
                value.TenantId = request.TenantId;
                value.Brand = request.Brand;
                value.Cpu = request.Cpu;
                value.EmployeeId = request.EmployeeId;
                value.IsEnabled = request.IsEnabled;
                value.StartPeriod = request.StartPeriod;
                value.StartYear = request.StartYear;
                value.EndPeriod = request.EndPeriod;
                value.EndYear = request.EndYear;

                await _dbContext.SaveChangesAsync();

                return CommandResult.Ok(value.Id);
            }
            catch (Exception ex)
            {
                return CommandResult.FromException(ex);
            }
        }
    }
}
