using MediatR;
using Nmbrs.Infrastructure.Log.Interfaces;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Nmbrs.Commands.Core.Result;

namespace Nmbrs.Computer.Service.Core.Commands.Behaviors
{
    public class LoggingBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TResponse : CommandResult
    {
        private readonly ILogger _logger;

        public LoggingBehavior(ILogger logger) => _logger = logger;

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            _logger.Information(CoreResources.LogCategory, $"Starting Handling {typeof(TRequest).Name}");
            
            var embeddedObjects = new Dictionary<string, object> {
                { "Request", request } ,
                { "RequestName", typeof(TRequest).Name }
            };

            var response = await next();

            if (!response.IsSuccessful)
                _logger.Error(CoreResources.LogCategory, $"Command result is not successful.", 
                                response.GetException(), embeddedObjects: embeddedObjects);

            _logger.Information(CoreResources.LogCategory, $"Handled Request", embeddedObjects: embeddedObjects);
            return response;
        }
    }
}
