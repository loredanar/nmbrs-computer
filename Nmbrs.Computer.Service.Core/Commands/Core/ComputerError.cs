using Nmbrs.Commands.Core.Result;

namespace Nmbrs.Computer.Service.Core.Commands.Core
{
    internal class ComputerError : ICommandError
    {
        public int Code { get; }
        public string Title { get; }
        public string Message { get; set; }
        public string Property { get; set; }

        public ComputerError(ComputerErrors errorCode, string message, string property)
        {
            Code = (int)errorCode;
            Title = errorCode.ToString();
            Message = message;
            Property = property;
        }
    }
}
