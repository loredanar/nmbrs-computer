using FluentValidation;

namespace Nmbrs.Computer.Service.Core.Commands.Validators
{
    public class UpdateNameCommandValidator : AbstractValidator<UpdateComputerCommand>
    {
        public UpdateNameCommandValidator()
        {
            RuleFor(model => model.Id).NotEmpty();
            RuleFor(model => model.UniqueId).NotEmpty();
            RuleFor(model => model.TenantId).NotEmpty();
            RuleFor(model => model.CompanyId).NotEmpty();
            RuleFor(model => model.Brand).NotEmpty();
            RuleFor(model => model.Cpu).NotEmpty();
            RuleFor(model => model.StartPeriod).NotEmpty();
            RuleFor(model => model.StartYear).NotEmpty();
            RuleFor(model => model.EndPeriod).NotEmpty();
            RuleFor(model => model.EndYear).NotEmpty();
        }
    }
}
