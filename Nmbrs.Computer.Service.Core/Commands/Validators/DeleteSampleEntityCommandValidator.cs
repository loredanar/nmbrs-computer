using FluentValidation;

namespace Nmbrs.Computer.Service.Core.Commands.Validators
{
    public class DeleteComputerCommandValidator : AbstractValidator<DeleteComputerCommand>
    {
        public DeleteComputerCommandValidator()
        {
            RuleFor(model => model.Id).NotEmpty();
        }
    }
}
