//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nmbrs.Computer.Service.Core {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class CoreResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal CoreResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Nmbrs.Computer.Service.Core.CoreResources", typeof(CoreResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Computer.Service.Core.
        /// </summary>
        internal static string ApplicationName {
            get {
                return ResourceManager.GetString("ApplicationName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to COMPUTER_GetAll_{0}_{1}.
        /// </summary>
        internal static string CACHE_KEY_SAMPLEENTITY_GETALL {
            get {
                return ResourceManager.GetString("CACHE_KEY_SAMPLEENTITY_GETALL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to COMPUTER_GetSingle_{0}.
        /// </summary>
        internal static string CACHE_KEY_SAMPLEENTITY_GETSINGLE {
            get {
                return ResourceManager.GetString("CACHE_KEY_SAMPLEENTITY_GETSINGLE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Computer Core.
        /// </summary>
        internal static string LogCategory {
            get {
                return ResourceManager.GetString("LogCategory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please ensure a model was supplied..
        /// </summary>
        internal static string ModelSupplied {
            get {
                return ResourceManager.GetString("ModelSupplied", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No fields to update..
        /// </summary>
        internal static string NoFieldsToUpdate {
            get {
                return ResourceManager.GetString("NoFieldsToUpdate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} with value {1} could not be found..
        /// </summary>
        internal static string NotFound {
            get {
                return ResourceManager.GetString("NotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to not provided..
        /// </summary>
        internal static string NotProvided {
            get {
                return ResourceManager.GetString("NotProvided", resourceCulture);
            }
        }
    }
}
