using AutoMapper;
using Nmbrs.Computer.Service.Client.Model.Internal;
using Nmbrs.Computer.Service.Client.Model.Request;

namespace Nmbrs.Computer.Service.Client.Services
{
    internal static class Automapper
    {
        internal static IMapper GetValuesMapper()
        {
            var configuration = new MapperConfiguration(config =>
            {
                config.CreateMap<CreateComputerRequest, CreateComputerModel>();
                config.CreateMap<CreateComputerRequest, CreateComputerModel>();
            });

            return new Mapper(configuration);
        }
    }
}
