using Nmbrs.Computer.Service.Client.Model.Request;
using Nmbrs.Computer.Service.Client.Model.Response;
using System.Threading.Tasks;

namespace Nmbrs.Computer.Service.Client
{
    public interface IComputerProvider
    {
        Task<ComputerCreatedResult> Create(CreateComputerRequest requestModel);
        void SetConnectionOptions(string endpointAddress, string apiKey, string instance);
    }
}
