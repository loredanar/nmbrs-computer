using System;

namespace Nmbrs.Computer.Service.Client
{
    public class ComputerProviderConfig
    {
        public string EndpointAddress { get; private set; }
        public string ApiKey { get; private set; }
        public string Instance { get; private set; }

        public ComputerProviderConfig(string endpointAddress, string apiKey) : this(endpointAddress, apiKey, null) { }

        public ComputerProviderConfig(string endpointAddress, string apiKey, string instance)
        {
            if (string.IsNullOrWhiteSpace(endpointAddress)) throw new ArgumentNullException(nameof(endpointAddress), "Missing endpoint address for the Computer Service client. Please review configurations");
            if (string.IsNullOrWhiteSpace(apiKey)) throw new ArgumentNullException(nameof(apiKey), "You must pass an api key to access the Computer service");

            EndpointAddress = endpointAddress;
            ApiKey = apiKey;
            Instance = instance;
        }
    }
}
