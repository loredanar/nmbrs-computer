using Nmbrs.Infrastructure.Http.Extensions;
using System;

namespace Nmbrs.Computer.Service.Client.Model.Request
{
    public class CreateComputerRequest : RequestModel
    {
        public string UniqueId { get; private set; }
        public int CompanyId { get; private set; }
        public Guid TenantId { get; private set; }
        public int Brand { get; private set; }
        public int Cpu { get; private set; }
        public int? EmployeeId { get; private set; }
        public bool IsEnabled { get; private set; }
        public int StartPeriod { get; private set; }
        public int StartYear { get; private set; }
        public int EndPeriod { get; private set; }
        public int EndYear { get; private set; }
        public CreateComputerRequest(Guid requestUser, string accessToken) : this(requestUser, accessToken, string.Empty) { }
        public CreateComputerRequest(Guid requestUser, string accessToken, string correlationId) : base(requestUser, accessToken, correlationId) { }
    }
}
