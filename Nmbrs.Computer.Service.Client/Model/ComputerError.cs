namespace Nmbrs.Computer.Service.Client.Model
{
    public class ComputerError
    {
        /// <summary>
        /// The error code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// The error description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The numeric code
        /// </summary>
        public ComputerErrorCodes NumericCode { get; set; }
    }
}
