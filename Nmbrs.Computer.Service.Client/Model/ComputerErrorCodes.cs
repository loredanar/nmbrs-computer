namespace Nmbrs.Computer.Service.Client.Model
{
    public enum ComputerErrorCodes
    {
        DefaultError = 1,
        NotAuthorized = 401,
        NotFound = 404
    }
}
