using System.Collections.Generic;

namespace Nmbrs.Computer.Service.Client.Model.Response
{
    public class ComputerCreatedResult : IOperationResult<ComputerIdResponse, OperationError>
    {
        public bool IsSuccessful { get; set; }
        public ComputerIdResponse Content { get; set; }
        public IEnumerable<OperationError> Errors { get; set; }

        public ComputerCreatedResult()
        {
            Errors = new List<OperationError>();
        }
    }

    public class ComputerIdResponse
    {
        public int Id { get; set; }
    }
}
