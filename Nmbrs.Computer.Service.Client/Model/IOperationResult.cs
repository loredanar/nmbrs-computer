using Nmbrs.Infrastructure.Http.Extensions;
using Nmbrs.Infrastructure.Http.Extensions.Exceptions;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Nmbrs.Computer.Service.Client.Model
{
    // move to Nmbrs.Infrastructure.Http.Extensions package once validated the usage in TenantService
    public interface IOperationResult<TSuccess, TError>
    {
        bool IsSuccessful { get; set; }
        TSuccess Content { get; set; }
        IEnumerable<TError> Errors { get; set; }
    }

    public class DefaultOperationResult : IOperationResult<object, OperationError>
    {
        public bool IsSuccessful { get; set; }
        public object Content { get; set; }
        public IEnumerable<OperationError> Errors { get; set; }

        public DefaultOperationResult()
        {
            Errors = new List<OperationError>();
        }
    }

    public class OperationError
    {
        public int Code { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Property { get; set; }

        public override string ToString()
        {
            return $"Code={Code}; Title={Title}; Message={Message}; Property={Property}";
        }
    }

    public static class Extensions
    {
        public static async Task<T> HandleGeneric<T, TSuccess, TError>(this HttpResponseMessage response) where T : IOperationResult<TSuccess, TError>, new()
        {
            switch (response.StatusCode)
            {
                case HttpStatusCode.NoContent:
                    return new T()
                    {
                        IsSuccessful = true
                    };
                case HttpStatusCode.OK:
                    return new T()
                    {
                        IsSuccessful = true,
                        Content = await response.ReadContentAs<TSuccess>()
                    };
                case HttpStatusCode.NotFound:
                case HttpStatusCode.InternalServerError:
                    return new T()
                    {
                        IsSuccessful = false,
                        Errors = new List<TError> { await response.ReadContentAs<TError>() }
                    };
                case HttpStatusCode.BadRequest:
                    return new T()
                    {
                        IsSuccessful = false,
                        Errors = await response.ReadContentAs<IEnumerable<TError>>()
                    };
                case HttpStatusCode.Unauthorized:
                    throw new UnauthorizedException(response.ReasonPhrase);
                case HttpStatusCode.Forbidden:
                    throw new ForbiddenException(response.ReasonPhrase);
                default:
                    throw new InvalidOperationException(response.ReasonPhrase);
            }
        }
    }
}
