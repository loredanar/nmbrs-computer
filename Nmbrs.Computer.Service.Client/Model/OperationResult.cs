using System.Collections.Generic;

namespace Nmbrs.Computer.Service.Client.Model
{
    public class OperationResult
    {
        /// <summary>
        /// Indicates if the operation was successfully executed or not
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// The identity errors if any
        /// </summary>
        public IEnumerable<ComputerError> Errors { get; set; }
    }
}
