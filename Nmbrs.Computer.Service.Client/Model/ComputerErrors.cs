namespace Nmbrs.Computer.Service.Client.Model
{
    public enum ComputerErrors
    {
        DefaultError = -1,
        InputError = -2,
        Exception = -3,
        NotFound = -4,
        NotAuthorized = -5,
    }
}
