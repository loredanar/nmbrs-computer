 using AutoMapper;
using Nmbrs.Infrastructure.Http.Extensions;
using Nmbrs.Computer.Service.Client.Model.Internal;
using Nmbrs.Computer.Service.Client.Model.Request;
using Nmbrs.Computer.Service.Client.Model.Response;
using Nmbrs.Computer.Service.Client.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Model = Nmbrs.Computer.Service.Client.Model;
using Nmbrs.Computer.Service.Client.Model;

namespace Nmbrs.Computer.Service.Client
{
    public class ComputerProvider : IComputerProvider
    {
        private static HttpClient _client;
        private readonly IMapper _mapper;

        private const string API = "api";
        private const string CONTROLLER = "tenant";
        private const string SCHEME = "Bearer";
        private const string ID = "id";
        private const string API_KEY_HEADER = "x-nmbrs-api-key";
        private const string INSTANCE_HEADER = "x-nmbrs-instance";
        private const string REQUEST_SOURCE_HEADER = "x-nmbrs-request-source";

        /// <summary>
        /// Creation of a ComputerProvider reciving an HttpClient from HttpClientFactory and DI. Recommended  for Services.
        /// </summary>
        /// <param name="httpClient"></param>
        /// <param name="config"></param>
        public ComputerProvider(HttpClient httpClient, ComputerProviderConfig config)
        {
            httpClient.BaseAddress = new Uri(config.EndpointAddress);
            httpClient.DefaultRequestHeaders.Add(API_KEY_HEADER, config.ApiKey);

            if (string.IsNullOrWhiteSpace(config.Instance))
                httpClient.DefaultRequestHeaders.Add(INSTANCE_HEADER, config.Instance);

            _client = httpClient;

            _mapper = Automapper.GetValuesMapper();
        }

        /// <summary>
        /// Classic Creation of ComputerProvider using only configurations. Recommended for Payroll.
        /// </summary>
        /// <param name="config"></param>
        public ComputerProvider(ComputerProviderConfig config, string executingAppName, string environment)
        {
            if (_client == null)
            {
                _client = new HttpClient()
                {
                    BaseAddress = new Uri(config.EndpointAddress)
                };
                _client.DefaultRequestHeaders.Add(API_KEY_HEADER, config.ApiKey);

                if (!string.IsNullOrEmpty(executingAppName) && !string.IsNullOrEmpty(environment))
                    _client.DefaultRequestHeaders.Add(REQUEST_SOURCE_HEADER, $"{executingAppName}/{environment}");

                if (!string.IsNullOrWhiteSpace(config.Instance))
                    _client.DefaultRequestHeaders.Add(INSTANCE_HEADER, config.Instance);
            }

            _mapper = Automapper.GetValuesMapper();
        }

        public void SetConnectionOptions(string endpointAddress, string apiKey, string instance)
        {
            if (string.IsNullOrWhiteSpace(endpointAddress)) throw new ArgumentNullException(nameof(endpointAddress), "Missing endpoint address for the Computer Service client. Please review configurations");
            if (string.IsNullOrWhiteSpace(apiKey)) throw new ArgumentNullException(nameof(apiKey), "You must pass an api key to access the Computer service");

            if (_client != null) _client.Dispose();

            _client = new HttpClient()
            {
                BaseAddress = new Uri(endpointAddress)
            };

            _client.DefaultRequestHeaders.Add(API_KEY_HEADER, apiKey);
            if (!string.IsNullOrWhiteSpace(instance)) _client.DefaultRequestHeaders.Add(INSTANCE_HEADER, instance);
        }

        public async Task<ComputerCreatedResult> Create(CreateComputerRequest requestModel)
        {
            if (requestModel == null) throw new ArgumentNullException(nameof(requestModel));

            var model = _mapper.Map<CreateComputerModel>(requestModel);
            var response = await _client.PostAsJson(GetUri(requestModel), model, requestModel.AccessToken).ConfigureAwait(false);

            return await response.HandleGeneric<ComputerCreatedResult, ComputerIdResponse, OperationError>().ConfigureAwait(false);
        }

        public Task<IEnumerable<Model.Computer>> GetValues(RequestModel requestModel)
        {
            return _client.Query<Model.Computer>(requestModel, () => $"{GetUri(requestModel)}");
        }

        private string GetUri(RequestModel requestModel)
        {
            return $"api/{requestModel.RequestUserId}/computer";
        }
    }
}
