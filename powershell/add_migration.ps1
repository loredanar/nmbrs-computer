$migrationName = Read-Host -Prompt 'Migration name (use ticket number)'
Write-Host "Creating migration '$migrationName'"
 
dotnet ef migrations add $migrationName --context ComputerDbContext --startup-project ../Nmbrs.Computer.DBInitializer/Nmbrs.Computer.DBInitializer.csproj --project ../Nmbrs.Computer.Service.Data/Nmbrs.Computer.Service.Data.csproj 
 